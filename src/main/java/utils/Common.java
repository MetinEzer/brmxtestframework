package utils;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.Reporter;

import pageFactory.cms.HomePage;
import pageFactory.cms.LoginPage;
import pageFactory.cms.contentPage.ContentPage;
import pageFactory.cms.contentPage.DocumentsPage;
import pageFactory.cms.contentPage.LabelsPage;
import pageFactory.cms.contentPage.MyProjectPage;
import pageFactory.site.SitePage;

public class Common {

	protected WebDriver driver;

	public static ArrayList<String> tabs;
	public static int testStepCount = 1;
	
	protected Credentials credentials;

	public static final String CMS_lANDING_PAGE = "http://localhost:8080/cms";
	public static final String SITE_lANDING_PAGE = "http://localhost:8080/site";
	public static final String EVENTS_PATH = "/content/path/content/documents/myproject/events/";
	public static final String SITE_BLOG_PAGE = "http://localhost:8080/site/blog";
	
	public static final String BLOG_ACCOUNT_TYPES = "account-types";
	public static final String BLOG_CATEGORIES = "blog-categories";
	public static final String BLOG = "blog";
	public static final String AUTHOR = "author";
	public static final String AUTHORS = "authors";
	public static final String EVENTS = "events";
	public static final String NEWS = "news";
	
	
	
	
	public static final int FIRST_TAB = 0;
	public static final int SECOND_TAB = 1;
	
	protected LoginPage loginPage;
	protected HomePage homePage;
	protected ContentPage contentPage;
	protected DocumentsPage documentsPage;
	protected LabelsPage labelsPage;
	protected MyProjectPage myProjectPage;
	protected SitePage sitePage;
		
	public static void LogInfo(String message) {
		Reporter.log(message, true);
	}
	
	public static void LogTestStep(String message) {
		LogInfo("===>>> Test Step " + testStepCount + ": " + message);
		testStepCount++;
	}
	
	public static void LogError(String message) {
		Reporter.log("!!!!!! Test Error" + message, true);
	}

	public static void LogKnownIssue(String message) {
		Reporter.log("!!!!!! KNOWN ISSUE: " + message, true);
	}

	public void loginGivenUser(String userName, String password) {
		
		loginPage.loginUser(userName, password);

		Assert.assertTrue(homePage.isHomePage(), "Login failed, home page is not loaded as expected");
		LogInfo("User " + "\"" + userName + "\"" + " logged in successfully!");
		homePage.uncollapseMenu();
	}

	public void logOutUser() {
		Utils.switchToDefaultContent(driver);
		loginPage = homePage.logOutFromCMS();
		
		Assert.assertTrue(loginPage.isLoginPage(), "Login page did not appeared!");
		LogInfo("Logged out successfully!");

	}
}
