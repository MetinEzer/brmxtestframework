package utils;

import static org.testng.Assert.assertFalse;

import java.util.ArrayList;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class Utils {

	public static void openNewTab(WebDriver driver, String directedURL) {
		
		Common.LogInfo("Opening new tab in the driver: " + directedURL);
		JavascriptExecutor js = (JavascriptExecutor) driver; 
		js.executeScript("window.open('" + directedURL + "','_blank');");
		Common.LogInfo("New tab opened successfully");
		
		Common.tabs = new ArrayList<String> (driver.getWindowHandles());
	}
	
	public static void switchTabs(WebDriver driver, int tabNumber) {
		Common.LogInfo("SwithcingTabs in the driver: " + "tabNumber: " + (tabNumber+1));
	    driver.switchTo().window(Common.tabs.get(tabNumber));
	}
	
	public static void switchFrame(WebDriver driver) {
		driver.switchTo().frame(driver.findElement(By.tagName("iframe")));
	}

	public static void switchToDefaultContent(WebDriver driver) {
		driver.switchTo().defaultContent(); 
	}
	
	public static void refreshPage(WebDriver driver) {
		driver.navigate().refresh();

		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		};

		Wait<WebDriver> wait = new WebDriverWait(driver, 5);
		try {
			wait.until(expectation);
		} catch (Throwable error) {
			Assert.fail("Timeout waiting for Page Load Request to complete");
		}
	}

	public static String generateRandomString() {
		return RandomStringUtils.randomAlphanumeric(8).toUpperCase();
	}
	
	public static String getCurrentURL(WebDriver driver) {
		return driver.getCurrentUrl();
	}
}


