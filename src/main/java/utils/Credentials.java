package utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Credentials {
	private final String adminUser;
	private final String adminPassword;
	private final String editorUser;
	private final String editorPassword;
	private final String authorUser;
	private final String authorPassword;
	
	public Credentials() throws FileNotFoundException, IOException {
		super();
		InputStream in = this.getClass().getResourceAsStream("/credentials");
		Properties credentials = new Properties();
		credentials.load(in);
		
		adminUser = credentials.getProperty("admin.user");
		editorUser = credentials.getProperty("editor.user");
		authorUser = credentials.getProperty("author.user");
		
		adminPassword = credentials.getProperty("admin.password");
		editorPassword = credentials.getProperty("editor.password");
		authorPassword = credentials.getProperty("author.password");
	}

	public String getAdminUser() {
		return adminUser;
	}

	public String getAdminPassword() {
		return adminPassword;
	}

	public String getEditorUser() {
		return editorUser;
	}

	public String getEditorPassword() {
		return editorPassword;
	}

	public String getAuthorUser() {
		return authorUser;
	}

	public String getAuthorPassword() {
		return authorPassword;
	}
}