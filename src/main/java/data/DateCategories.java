package data;

public enum DateCategories {
	LastSevenDays,
	LastMonth,
	LastThreeMonths,
	LastSixMonths,
	LastYear;
}
