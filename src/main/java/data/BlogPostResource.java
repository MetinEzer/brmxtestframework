package data;

import java.util.Calendar;
import java.util.List;

import utils.Utils;

public class BlogPostResource {
	private String blogPostName;
	private String title;
	private String introduction;
	private String content;
	private List<String> existingAuthorNames;
	private List<String> existingCategories;
	private Calendar publicationDate;
	
	public String getBlogPostName() {
		return blogPostName;
	}
	public String getTitle() {
		return title;
	}
	public String getIntroduction() {
		return introduction;
	}
	public String getContent() {
		return content;
	}
	public List<String> getExistingAuthorNames() {
		return existingAuthorNames;
	}
	public List<String> getExistingCategories() {
		return existingCategories;
	}
	
	public Calendar getPublicationDate() {
		return publicationDate;
	}

	public void setBlogPostName(String blogPostName) {
		this.blogPostName = blogPostName;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setExistingAuthorNames(List<String> existingAuthorNames) {
		this.existingAuthorNames = existingAuthorNames;
	}

	public void setExistingCategories(List<String> existingCategories) {
		this.existingCategories = existingCategories;
	}

	public void setPublicationDate(Calendar publicationDate) {
		this.publicationDate = publicationDate;
	}

	public static BlogPostResource createDefaultBlogPostDetail(List<String> existingAuthor, List<String> existingCategories) {
		BlogPostResource blogPostResource = new BlogPostResource();
		
		blogPostResource.blogPostName = "blogPostName_" + Utils.generateRandomString();
		blogPostResource.title = "title_" + Utils.generateRandomString();
		blogPostResource.introduction = "introduction_" + Utils.generateRandomString();
		blogPostResource.content = "content_" + Utils.generateRandomString();
		blogPostResource.existingAuthorNames = existingAuthor;
		blogPostResource.existingCategories = existingCategories;
		blogPostResource.publicationDate = Calendar.getInstance();
		blogPostResource.publicationDate.add(Calendar.DATE, 1);

		
		return blogPostResource;
	}
}
