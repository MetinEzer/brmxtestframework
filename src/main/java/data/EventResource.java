package data;

import java.util.Calendar;

import utils.Utils;


public class EventResource {

	private String eventDocumentName;
	private String title;
	private String introduction;
	private String content;
	private String location;
	private Calendar startDate;
	private Calendar endDate;

	public String getEventDocumentName() {
		return eventDocumentName;
	}

	public String getTitle() {
		return title;
	}

	public String getIntroduction() {
		return introduction;
	}

	public String getContent() {
		return content;
	}

	public String getLocation() {
		return location;
	}

	public Calendar getStartDate() {
		return startDate;
	}

	public Calendar getEndDate() {
		return endDate;
	}

	public static EventResource createDefaultEventDetail() {
		EventResource event = new EventResource();
		
		event.eventDocumentName = "documentName_" + Utils.generateRandomString();
		event.title = "title_" + Utils.generateRandomString();
		event.introduction = "introduction_" + Utils.generateRandomString();
		event.content = "content_" + Utils.generateRandomString();
		event.location = "location_" + Utils.generateRandomString();
		
		event.startDate = Calendar.getInstance();
		event.startDate.add(Calendar.DATE, 1);
		
		event.endDate = Calendar.getInstance();
		event.endDate.add(Calendar.DATE, 2);

		return event;
	}
}
