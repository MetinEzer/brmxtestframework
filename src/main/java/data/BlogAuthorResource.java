package data;

import utils.Utils;

public class BlogAuthorResource {
	private String name;
	private String role;
	private String description;
	private String existingAccount;
	private String externalLink;
	
	public String getName() {
		return name;
	}
	public String getRole() {
		return role;
	}
	public String getExistingAccount() {
		return existingAccount;
	}

	public String getExternalLink() {
		return externalLink;
	}
	
	
	public String getDescription() {
		return description;
	}
	
	public static BlogAuthorResource createDefaultBlogAuthorResource(String existingAccount) {
		BlogAuthorResource resource = new BlogAuthorResource();
		
		resource.name = "authorName_" + Utils.generateRandomString();
		resource.role = "role_" + Utils.generateRandomString();
		resource.description = "description_" + Utils.generateRandomString();
		resource.existingAccount = existingAccount;
		resource.externalLink = "https://www.bloomreach.com/en";
		
		return resource;
	}
}
