package data;

import java.util.Calendar;

import utils.Utils;

public class NewsItemResource {
	private String newsItemName;
	private String title;
	private String introduction;
	private String content;
	private String location;
	private String author;
	private String source;
	private Calendar date;
	
	public String getNewsItemName() {
		return newsItemName;
	}
	public String getTitle() {
		return title;
	}
	public String getIntroduction() {
		return introduction;
	}
	public String getContent() {
		return content;
	}
	public String getLocation() {
		return location;
	}
	public String getAuthor() {
		return author;
	}
	public String getSource() {
		return source;
	}
	public Calendar getDate() {
		return date;
	}
	
	public static NewsItemResource createDefaultNewsItemDetail() {
		NewsItemResource newsItem = new NewsItemResource();
		
		newsItem.newsItemName = "documentName_" + Utils.generateRandomString();
		newsItem.title = "title_" + Utils.generateRandomString();
		newsItem.introduction = "introduction_" + Utils.generateRandomString();
		newsItem.content = "content_" + Utils.generateRandomString();
		newsItem.location = "location_" + Utils.generateRandomString();
		newsItem.author = "author_" + Utils.generateRandomString();
		newsItem.source = "source_" + Utils.generateRandomString();
		
		newsItem.date = Calendar.getInstance();
		newsItem.date.add(Calendar.DATE, 1);

		return newsItem;
	}
}