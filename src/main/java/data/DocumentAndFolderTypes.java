package data;

public enum DocumentAndFolderTypes {
	  Event,
	  News,
	  BlogAuthor,
	  Blog,
	  Content;
}
