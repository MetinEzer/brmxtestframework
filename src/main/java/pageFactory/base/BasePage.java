package pageFactory.base;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import pageFactory.cms.HomePage;
import pageFactory.cms.LoginPage;
import pageFactory.cms.contentPage.ContentPage;
import pageFactory.cms.experiencePage.ExperienceManagerPage;

public class BasePage {
	public WebDriver driver;

	//side menu items
	@FindBy(xpath = "//div[contains(text(),'Home')]")
	WebElement homeButton;
	
	@FindBy(xpath = "//div[contains(text(),'Experience manager')]")
	WebElement experienceManager;
	
	@FindBy(xpath = "//div[contains(text(),'Content')]")
	WebElement content;
	
	@FindBy(xpath = "//div[contains(text(),'Setup')]")
	WebElement setup;
	
	@FindBy(xpath = "//div[contains(text(),'User account')]")
	WebElement userAccount;
	
	@FindBy(xpath = "//body/brna-root[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/nav[1]/brna-main-menu[1]/div[1]/div[3]/div[1]")
	WebElement collapseMenu;
	
	@FindBy(xpath = "//a[contains(text(),'Logout')]")
	WebElement logOut;
	
	public BasePage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void uncollapseMenu() {
		collapseMenu.click();
	}
	
	public HomePage goToHomePage() {
		homeButton.click();
		return new HomePage(driver);
	}
	
	public ExperienceManagerPage goToExperienceManagerPage() {
		experienceManager.click();
		return new ExperienceManagerPage(driver);
	}

	public ContentPage goToContentPage() {
		content.click();
		return new ContentPage(driver);
	}

	public LoginPage logOutFromCMS() {
		userAccount.click();
		
		try {
			logOut.click();	
			} 
		catch (org.openqa.selenium.ElementClickInterceptedException e) {
			JavascriptExecutor executor = (JavascriptExecutor) driver;
		    executor.executeScript("arguments[0].click();", driver.findElement(By.xpath("//a[contains(text(),'Logout')]")));
		    }
		
		return new LoginPage(driver);
	}
}
