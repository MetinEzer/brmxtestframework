package pageFactory.site;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import data.BlogPostResource;
import data.DateCategories;
import data.SiteTabs;
import pageFactory.base.BasePage;

public class SitePage extends BasePage{

	@FindBy(xpath = "/html[1]/body[1]/div[1]/div[4]/div[2]")
	WebElement siteGlobalFooterValue;
	
	@FindBy(xpath = "//a[@href = '/site/']")
	WebElement homePageTab;
	
	@FindBy(xpath = "//a[@href = '/site/blog']")
	WebElement blogPageTab;

	@FindBy(xpath = "//a[@href = '/site/events']")
	WebElement eventsPageTab;
	
	@FindBy(xpath = "//a[@href = '/site/news']")
	WebElement newsPageTab;
	
	@FindBy(xpath = "//a[@href = '/site/content']")
	WebElement contentPageTab;
	
	@FindBy(xpath = "//p")
	List<WebElement> eventElements;
	
	@FindBy(xpath = "//a[text() = 'next']")
	WebElement nextButton;
	
	@FindBy(xpath = "//div[@class='col-md-9']//h1")
	WebElement blogPostTitle;
	
	@FindBy(xpath = "//div[@class='col-md-9']//h2")
	WebElement blogPostAuthor;
	
	@FindBy(xpath = "//div[@class='col-md-9']//strong")
	WebElement blogPostPublicationDate;
	
	@FindBy(xpath = "//div[@class='col-md-9']//p")
	List<WebElement> blogPostIntroductionAndContent;
	
	@FindBy(xpath = "//a[contains(text(),'< Back to blog posts')]")
	WebElement backToBlockPostsButton;
	
	public SitePage(WebDriver driver){
		super(driver);
		PageFactory.initElements(driver, this);
	}

	/**
	 * Returns site global footer value
	 * @return String
	 */
	public String getSiteGlobalFooterValue() {
		return siteGlobalFooterValue.getText();
	}
	
	/**
	 * Clicks given tab name
	 * @param tabName
	 * @return SitePage
	 */
	public SitePage clickGivenTab(SiteTabs tabName) {
		
		if(tabName == SiteTabs.Home)
			homePageTab.click();
		else if(tabName == SiteTabs.Blog)
			blogPageTab.click();
		else if (tabName == SiteTabs.Event)
			eventsPageTab.click();
		else if (tabName == SiteTabs.News)
			newsPageTab.click();
		else if (tabName == SiteTabs.Content)
			contentPageTab.click();
		
		return this;
	}
	
	/**
	 * Checks given blog post exist
	 * @param blogPostTitle
	 * @return boolean
	 */
	public boolean isBlogPostExist(String blogPostTitle) {
		boolean flag = false;
		
		List<WebElement> visibleBlogPosts = driver.findElements(By.xpath("//article//h3//a"));
		
		new WebDriverWait(driver, 5).ignoring(StaleElementReferenceException.class)
		.until(ExpectedConditions.visibilityOfAllElements(visibleBlogPosts));

		while(!flag) {
			for(WebElement element : visibleBlogPosts)
				if(blogPostTitle.equals(element.getText())) {
					flag = true;
					return flag;
				}
			if(nextButton.isDisplayed()) {
				nextButton.click();
				visibleBlogPosts = driver.findElements(By.xpath("//article//h3//a"));
			}
			else
				return flag;
		}
		
		return flag;
	}
	
	/**
	 * Checks given author exists
	 * @param authorName
	 * @return boolean
	 */
	public boolean isAuthorExists(String authorName) {
		return driver.findElement(By.xpath("//a[contains(@href,'" + authorName + "')]")).isDisplayed();
	}
	
	/**
	 * Clicks given author name
	 * @param authorName
	 * @return SitePage
	 */
	public SitePage clickAuthor(String authorName) {
		driver.findElement(By.xpath("//a[contains(@href,'" + authorName + "')]")).click();
		return this;
	}
	
	/**
	 * Checks blog post is under correct author filter
	 * @param authorName
	 * @param blogPostTitle
	 * @return boolean
	 */
	public boolean isBlogPostUnderCorrectAuthor(String authorName, String blogPostTitle) {
		clickAuthor(authorName);
		return isBlogPostExist(blogPostTitle);
	}

	/**
	 * Checks category exists
	 * @param categoryName
	 * @return boolean
	 */
	public boolean isCategoryExists(String categoryName) {
		return driver.findElement(By.xpath("//a[contains(@href,'" + categoryName + "')]")).isDisplayed();	
	}

	/**
	 * Clicks given category
	 * @param categoryName
	 * @return SitePage
	 */
	public SitePage clickCategory(String categoryName) {
		driver.findElement(By.xpath("//a[contains(@href,'" + categoryName + "')]")).click();
		return this;
	}
	
	/**
	 * Checks blog post under correct category
	 * @param categoryName
	 * @param blogPostTitle
	 * @return boolean
	 */
	public boolean isBlogPostUnderCorrectCategory(String categoryName, String blogPostTitle) {
		clickCategory(categoryName);
		return isBlogPostExist(blogPostTitle);
	}
	
	/**
	 * Clicks given date category
	 * @param category
	 * @return SitePage
	 */
	public SitePage clickDate(DateCategories category) {
		if(category == DateCategories.LastSevenDays)
			driver.findElement(By.xpath("//a[contains(@href,'Last+7+days')]")).click();
		else if(category == DateCategories.LastMonth)
			driver.findElement(By.xpath("//a[contains(@href,'Last+month')]")).click();
		else if(category == DateCategories.LastThreeMonths)
			driver.findElement(By.xpath("//a[contains(@href,'Last+3+months')]")).click();
		else if(category == DateCategories.LastSixMonths)
			driver.findElement(By.xpath("//a[contains(@href,'Last+6+months')]")).click();
		else if(category == DateCategories.LastYear)
			driver.findElement(By.xpath("//a[contains(@href,'Last+year')]")).click();
		return this;
	}
	
	/**
	 * Checks blog post is under correct date category
	 * @param category
	 * @param blogPostTitle
	 * @return boolean
	 */
	public boolean isBlogPostUnderCorrectDate(DateCategories category , String blogPostTitle) {
		clickDate(category);
		return isBlogPostExist(blogPostTitle);
	}
	
	/**
	 * Unselects filter
	 * @return SitePage
	 */
	public SitePage unselectFilter() {
		driver.findElement(By.xpath("//span[@class = 'alert-danger']")).click();
		return this;
	}
	
	/**
	 * Checks event is present
	 * @param eventTitle
	 * @return boolean
	 */
	public boolean isEventPresent(String eventTitle) {
		return driver.findElement(By.xpath("//a[contains(text(),'"+ eventTitle + "')]" )).isDisplayed();
	}
	
	/**
	 * Go to event document
	 * @param eventTitle
	 * @return SitePage
	 */
	public SitePage goToEventDocument(String eventTitle) {
		WebElement element = driver.findElement(By.xpath("//a[contains(text(),'"+ eventTitle + "')]" ));
		element.click();
		return this;
	}
	
	/**
	 * Get start date
	 * @return String
	 */
	public String getStartDate() {
		return eventElements.get(0).getText();
	}
	
	/**
	 * Get end date
	 * @return String
	 */
	public String getEndDate() {
		return eventElements.get(1).getText();
	}

	/**
	 * Get location
	 * @return String
	 */
	public String getLocation() {
		return eventElements.get(2).getText();
	}

	/**
	 * Get introduction
	 * @return String
	 */
	public String getIntroduction() {
		return eventElements.get(3).getText();
	}

	/**
	 * Get content
	 * @return String
	 */
	public String getContent() {
		return eventElements.get(4).getText();
	}
	
	/**
	 * Clicks blog post
	 * @param String
	 * @return SitePage
	 */
	public SitePage clickBlogPost(String blogPostTitle) {
		driver.findElement(By.xpath("//a[text() = '" + blogPostTitle + "']")).click();
		return this;
	}
	
	/**
	 * Returns blog post title
	 * @return String
	 */
	public String getBlogPostTitle() {
		return blogPostTitle.getText();
	}
	
	/**
	 * Returns blog post author
	 * @return String
	 */
	public String getBlogPostAuthor() {
		return blogPostAuthor.getText();
	}
	
	/**
	 * Returns blog post publication date
	 * @return String
	 */
	public String getBlogPostPublicationDate() {
		return blogPostPublicationDate.getText();
	}
	
	/**
	 * Returns blog post introduction
	 * @return String
	 */
	public String getBlogPostIntroduction() {
		return blogPostIntroductionAndContent.get(0).getText();
	}
	
	/**
	 * Returns blog post content
	 * @return String
	 */
	public String getBlogPostContent() { 
		return blogPostIntroductionAndContent.get(1).getText();
	}
	
	/**
	 * Clicks back to blog posts button
	 * @return SitePage
	 */
	public SitePage backToBlogPosts() {
		backToBlockPostsButton.click();
		return this;
	}
}
