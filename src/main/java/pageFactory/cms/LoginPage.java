package pageFactory.cms;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import pageFactory.base.BasePage;

public class LoginPage extends BasePage {

	@FindBy(xpath = "//input[@class = 'login-form-input-username']")
	WebElement userNameElement;

	@FindBy(xpath = "//input[@class = 'login-form-input-password']")
	WebElement passwordElement;

	@FindBy(xpath = "//span[@class = 'login-form-submit-label']")
	WebElement loginButton;

	@FindBy(xpath = "//div[@class='login-panel']")
	WebElement loginPageIndicator;
	
	public LoginPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	/**
	 * Sends the given name to user name field
	 * @param name
	 * @return LoginPage
	 */
	public LoginPage setUserName(String name) {
		userNameElement.sendKeys(name);
		return this;
	}

	/**
	 * Sends the given password to password field
	 * @param password
	 * @return LoginPage
	 */
	public LoginPage setPassword(String password) {
		passwordElement.sendKeys(password);
		return this;
	}

	/**
	 * Clicks login button of CMS
	 */
	public void clickLoginButton() {
		loginButton.click();
	}
	
	/**
	 * Enters given user name and password and logs in
	 * @param name
	 * @param password
	 */
	public void loginUser(String name, String password) {
		setUserName(name).setPassword(password).clickLoginButton();
	}
	
	/**
	 * Checks if the page is login page
	 * @return boolean
	 */
	public boolean isLoginPage() {
		return loginPageIndicator.isDisplayed();
	}
	
}
