package pageFactory.cms.contentPage;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import data.BlogAuthorResource;
import data.BlogPostResource;
import data.DocumentAndFolderTypes;
import data.EventResource;
import data.NewsItemResource;

public class MyProjectPage extends DocumentsPage {

	@FindBy(xpath = "//span[contains(text(),'events')]")
	WebElement eventsFolder;

	@FindBy(xpath = "//span[contains(text(),'events')]//..//..//..//a[@class = 'hippo-tree-dropdown-icon-container container-selected']")
	WebElement eventsDropDownContainer;

	@FindBy(xpath = "//span[contains(text(),'Add new event...')]")
	WebElement addNewEvent;

	@FindBy(xpath = "//div[@class='hippo-editor-field']//span[text() = 'Introduction']//..//..//textarea")
	WebElement introduction;

	@FindBy(xpath = "//div[@class='hippo-editor-field']//span[text() = 'Content']//..//..//div[@role = 'textbox']")
	WebElement content;

	@FindBy(xpath = "//div[@class='hippo-editor-field']//span[text() = 'Start date']//..//..//div[@class = 'hippo-datepicker-date']//input")
	WebElement startDate;

	@FindBy(xpath = "//div[@class='hippo-editor-field']//span[text() = 'Start date']//..//..//div[@class = 'hippo-datepicker-value-container hippo-datepicker-hours']//input")
	WebElement startHours;

	@FindBy(xpath = "//div[@class='hippo-editor-field']//span[text() = 'Start date']//..//..//div[@class = 'hippo-datepicker-value-container hippo-datepicker-minutes']//input")
	WebElement startMinutes;

	@FindBy(xpath = "//div[@class='hippo-editor-field']//span[text() = 'End date']//..//..//div[@class = 'hippo-datepicker-date']//input")
	WebElement endDate;

	@FindBy(xpath = "//div[@class='hippo-editor-field']//span[text() = 'End date']//..//..//div[@class = 'hippo-datepicker-value-container hippo-datepicker-hours']//input")
	WebElement endHours;

	@FindBy(xpath = "//div[@class='hippo-editor-field']//span[text() = 'End date']//..//..//div[@class = 'hippo-datepicker-value-container hippo-datepicker-minutes']//input")
	WebElement endMinutes;

	@FindBy(xpath = "//a[@class = 'hippo-datepicker-reset btn btn-default']")
	List<WebElement> setToNowButtons;

	@FindBy(xpath = "//div[@class='hippo-editor-field']//span[text() = 'Location']//..//..//input")
	WebElement location;

	@FindBy(xpath = "//span[contains(text(),'Add new events folder...')]")
	WebElement addNewEventFolder;

	@FindBy(xpath = "//div[@class = 'input']//input[@type = 'text']")
	WebElement newNameTextBox;

	@FindBy(xpath = "//span[contains(text(),'News')]")
	WebElement newsFolder;
	
	@FindBy(xpath = "//span[contains(text(),'Add new news item...')]")
	WebElement addNewNewsItem;
	
	@FindBy(xpath = "//div[@class='hippo-editor-field']//span[text() = 'Author']//..//..//input")
	WebElement newsItemAuthorBox;
	
	@FindBy(xpath = "//div[@class='hippo-editor-field']//span[text() = 'Source']//..//..//input")
	WebElement newsItemSourceBox;

	@FindBy(xpath = "//div[@class='hippo-editor-field']//span[text() = 'Location']//..//..//input")
	WebElement newsItemLocationBox;

	@FindBy(xpath = "//span[contains(text(),'Add new blog author...')]")
	WebElement addNewBlogAuthor;
	
	@FindBy(xpath = "//span[contains(text(),'Add new blog post...')]")
	WebElement addNewBlogPost;

	@FindBy(xpath = "//span[contains(text(),'Add new content document...')]")
	WebElement addNewContentDocument;
	
	@FindBy(xpath = "//span[contains(text(),'Add new blog folder...')]")
	WebElement addNewBlogFolder;
	
	@FindBy(xpath = "//span[contains(text(),'Add new blog author folder...')]")
	WebElement addNewBlogAuthorFolder;
	
	@FindBy(xpath = "//span[contains(text(),'Add new content folder...')]")
	WebElement addNewContentFolder;
	
	@FindBy(xpath = "//span[contains(text(),'Add new news item folder...')]")
	WebElement addNewNewsItemFolder;
	
	@FindBy(xpath = "//div[@class='hippo-editor-field']//span[text() = 'Publication Date']//..//..//div[@class = 'hippo-datepicker-date']//input")
	WebElement blogPostPublicationDateBox;
	
	@FindBy(xpath = "//span[text() = 'Select']")
	List<WebElement> selectAuthorButtons;
	
	@FindBy(xpath = "//span[text() = 'account-types']")
	WebElement blogAccountTypeDocument;
	
	@FindBy(xpath = "//span[text() = 'blog-categories']")
	WebElement blogCategoriesDocument;
	
	@FindBy(xpath = "//span[contains(text(),'Title')]//..//..//div[@class = 'hippo-editor-field-value-container']")
	List<WebElement> documentTitle;

	@FindBy(xpath = "//span[contains(text(),'Introduction')]//..//..//div[@class = 'hippo-editor-field-textarea']//span")
	List<WebElement> documentIntroduction;

	@FindBy(xpath = "//span[contains(text(),'Content')]//..//..//div[@class = 'hippo-richtext']")
	List<WebElement> documentContent;

	@FindBy(xpath = "//div[@class='hippo-editor-field-datepicker']//span")
	List<WebElement> eventStartAndEndDate;

	@FindBy(xpath = "//span[contains(text(),'Location')]//..//..//div[@class='hippo-editor-field-value-container']")
	List<WebElement> documentLocation;
	
	@FindBy(xpath = "//div[@class='hippo-editor-field']//span[text() = 'Date']//..//..//div[@class = 'hippo-datepicker-date']//input")
	WebElement newsItemDateBox;
	
	@FindBy(xpath = "//div[@class='hippo-editor-field']//span[text() = 'Title']//..//..//input")
	List<WebElement> title;
	
	@FindBy(xpath = "//span[contains(text(),'Author')]//..//..//div[@class='hippo-editor-field-value-container']")
	List<WebElement> newsItemAuthor;
	
	@FindBy(xpath = "//span[contains(text(),'Source')]//..//..//div[@class='hippo-editor-field-value-container']")
	List<WebElement> newsItemSource;
	
	@FindBy(xpath = "//span[text() = 'Name']//..//..//div[@class = 'hippo-editor-field-value-container']//input")
	WebElement authorNameBox;
	
	@FindBy(xpath = "//span[text() = 'Name']//..//..//div[@class = 'hippo-editor-field-value-container']")
	WebElement authorName;
	
	@FindBy(xpath = "//span[text() = 'Role']//..//..//div[@class = 'hippo-editor-field-value-container']//input")
	WebElement authorRoleBox;
	
	@FindBy(xpath = "//span[text() = 'Role']//..//..//div[@class = 'hippo-editor-field-value-container']")
	WebElement authorRole;
	
	@FindBy(xpath = "//span[text() = 'Description']//..//..//div[@role = 'textbox']")
	WebElement authorDescriptionBox;
	
	@FindBy(xpath = "//span[text() = 'Description']//..//..//p")
	WebElement authorDescription;

	@FindBy(xpath = "//select[@class = 'dropdown-plugin']")
	WebElement authorAccountTypesDropdownBox;
	
	@FindBy(xpath = "//span[text() = 'Type']//..//..//div[@class = 'hippo-editor-field-subfield-item']//span")
	WebElement authorAccountType;
	
	@FindBy(xpath = "//span[text() = 'Link']//..//..//div[@class = 'hippo-editor-field-value-container']//input")
	WebElement authorLinkBox;
	
	@FindBy(xpath = "//span[text() = 'Link']//..//..//div[@class = 'hippo-editor-field-value-container']")
	WebElement authorLink;
	
	@FindBy(xpath = "//span[contains(text(),'Author(s)')]//..//..//span[@class ='hippo-editor-field-value-container']//span")
	List<WebElement> blogPostAuthors;
	
	@FindBy(xpath = "//span[contains(text(),'Publication Date')]//..//..//div[@class = 'hippo-editor-field-datepicker']//span")
	WebElement blogPostPublicationDate;
	
	@FindBy(xpath = "//span[contains(text(),'Categories')]//..//..//ul//span")
	List<WebElement> blogPostCategory;
	
	public MyProjectPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	/**
	 * Clicks given folder name
	 * @param folderName
	 * @return MyProjectPage
	 */
	public MyProjectPage clickGivenFolder(String folderName) {
		
		WebElement element = driver.findElement(By.xpath("//div[@class = 'a_']//span[contains(text(),'" + folderName + "')]"));
		
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(element));
		element.click();
		
		return this;
	}
	
	/**
	 * Clicks given document name
	 * @param documentName
	 * @return MyProjectPage
	 * @throws InterruptedException 
	 */
	public MyProjectPage clickGivenDocument(String documentName) throws InterruptedException {
		Thread.sleep(1000);
		WebElement element = driver.findElement(By.xpath("//div[@class = 'hi hi-stack hi-l']//..//..//..//..//td//span[contains(text(),'" + documentName + "')]"));
		
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(element));
		
		element.click();

		return this;
	}
	
	/**
	 * Creates a new document under given folder
	 * @param documentType
	 * @param folderName
	 * @param documentName
	 * @return MyProjectPage
	 * @throws InterruptedException
	 */
	public MyProjectPage createNewDocumentUnderGivenFolder(DocumentAndFolderTypes documentType, String folderName, String documentName) throws InterruptedException {
		openFolderOption(folderName);
		
		if(documentType == DocumentAndFolderTypes.Blog)
			addNewBlogPost.click();
		else if(documentType == DocumentAndFolderTypes.BlogAuthor)
			addNewBlogAuthor.click();
		else if (documentType == DocumentAndFolderTypes.Event)
			addNewEvent.click();
		else if (documentType == DocumentAndFolderTypes.News)
			addNewNewsItem.click();
		else if (documentType == DocumentAndFolderTypes.Content)
			addNewContentDocument.click();
		
		newNameTextBox.sendKeys(documentName);
		submitFormButton.click();

		return this;
	}
	
	/**
	 * Creates a new folder under given folder
	 * @param folderType
	 * @param parentFolder
	 * @param folderName
	 * @return
	 * @throws InterruptedException
	 */
	public MyProjectPage createNewFolderUnderGivenFolder(DocumentAndFolderTypes folderType, String parentFolder, String folderName) throws InterruptedException
	{
		openFolderOption(parentFolder);

		if(folderType == DocumentAndFolderTypes.Blog)
			addNewBlogFolder.click();
		else if(folderType == DocumentAndFolderTypes.BlogAuthor)
			addNewBlogAuthorFolder.click();
		else if (folderType == DocumentAndFolderTypes.Event)
			addNewEventFolder.click();
		else if (folderType == DocumentAndFolderTypes.News)
			addNewNewsItemFolder.click();
		else if (folderType == DocumentAndFolderTypes.Content)
			addNewContentFolder.click();

		newNameTextBox.sendKeys(folderName);
		submitFormButton.click();

		return this;
	}

	/**
	 * Opens given folder options
	 * @param folderName
	 * @throws InterruptedException
	 */
	private void openFolderOption(String folderName) throws InterruptedException {
		Thread.sleep(1000);
		
		WebElement folder = driver.findElement(By.xpath("//div[@class = 'a_']//span[contains(text(),'" + folderName + "')]"));

		Actions builder = new Actions(driver);
		builder.moveToElement(folder).perform();

		WebElement container = driver.findElement(By.xpath("//span[contains(text(),'" + folderName
				+ "')]//..//..//..//a[@class = 'hippo-tree-dropdown-icon-container container-selected']"));
		builder.click(container).perform();
	}
	
	/**
	 * Check if folder exists
	 * @param folderName
	 * @return boolean
	 */
	public boolean isFolderCreated(String folderName) {
		return driver.findElement(By.xpath("//span[contains(text(),'" + folderName + "')]")).isDisplayed();
	}
	
	/**
	 * Check if document exists
	 * @param documentName
	 * @return boolean
	 */
	public boolean isDocumentCreated(String documentName) {
		WebElement element = driver
				.findElement(By.xpath("//span[@class= 'hippo-document']//span[contains(text(),'" + documentName + "')]"));
		return element.isDisplayed();
	}
	
	/**
	 * Takes offline all documents under given folder 
	 * and checks if the number of live documents shown correctly on the pop-up window
	 * 
	 * @param folderName
	 * @param numberOfPublishedDocuments
	 * @return MyProjectPage
	 * @throws InterruptedException
	 */
	public MyProjectPage takeOfflineAllDocumentsUnderGivenFolder(String folderName, int numberOfPublishedDocuments)
			throws InterruptedException {
		openFolderOption(folderName);

		takeOfflineAllInFolder.click();
			
		Assert.assertTrue(driver
				.findElement(
						By.xpath("//td[text() = 'Folder to operate on:']//..//span[text() = '" + folderName + "']"))
				.isDisplayed(), "Wrong folder to operate on!");
		Assert.assertTrue(
				driver.findElement(
						By.xpath("//span[text() = 'Number of documents to take offline:']//..//..//span[text() = '"
								+ String.valueOf(numberOfPublishedDocuments) + "']"))
						.isDisplayed(),
				"Wrong number of document to take offline!");
		
		submitFormButton.click();
		doneFormButton.click();
		
		return this;
	}

	/**
	 * Deletes given folder
	 * @param folderName
	 * @return DocumentsPage
	 * @throws InterruptedException 
	 */
	public MyProjectPage deleteFolder(String folderName) throws InterruptedException {
		openFolderOption(folderName);
		
		deleteFolder.click();
		submitFormButton.click();
		
		return this;
	}
	
	/**
	 * Checks if given folder is deleted
	 * @param folderName
	 * @return boolean
	 */
	public boolean isFolderDeleted(String folderName) {
		try{
			driver.switchTo().frame(driver.findElement(By.tagName("iframe")));
			return driver.findElement(By.xpath("//span[text() = '" + folderName + "')]")).isDisplayed();
		}
		catch(NoSuchElementException e) {
			return true;
		}
	}

	/**
	 * Verifies folder cannot be deleted if it is not empty
	 * @param folderName
	 * @return boolean
	 * @throws InterruptedException
	 */
	public boolean cannotDeleteFolder(String folderName) throws InterruptedException {
		openFolderOption(folderName);
		deleteFolder.click();

		WebElement cannotBeDeleted = driver.findElement(By.xpath("//span[contains(text(),\"The folder '" + folderName + "' is not empty\")]"));
		
		if(cannotBeDeleted.isDisplayed()) {
			cancelFormButton.click();
			return true;
		}
		return false;
	}

	/**
	 * Fills created event document details and saves
	 * @param EventResource resource
	 * @return MyProjectPage
	 * @throws InterruptedException
	 */
	public MyProjectPage insertAndSaveEventDetails(EventResource resource) throws InterruptedException {

		this.title.get(0).sendKeys(resource.getTitle());
		this.introduction.sendKeys(resource.getIntroduction());
		this.content.sendKeys(resource.getContent());

		this.startDate.sendKeys(dateFormatter(resource.getStartDate()));
		this.startHours.sendKeys(getHours(resource.getStartDate()));
		this.startMinutes.sendKeys(getMinutes(resource.getStartDate()));

		this.endDate.sendKeys(dateFormatter(resource.getEndDate()));
		this.endHours.sendKeys(getHours(resource.getEndDate()));
		this.endMinutes.sendKeys(getMinutes(resource.getEndDate()));

		this.location.sendKeys(resource.getLocation());

		doneChanges();

		Thread.sleep(2000);

		List<WebElement> offlineIndicators = driver.findElements(By.xpath("//span[contains(text(),'Offline')]"));
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(offlineIndicators.get(offlineIndicators.size() - 1)));

		return this;
	}
	
	/**
	 * Fills created news item details and saves
	 * @param NewsItemResource
	 * @return MyProjectPage
	 * @throws InterruptedException
	 */
	public MyProjectPage insertAndSaveNewsDetails(NewsItemResource resource) throws InterruptedException {

		this.title.get(0).sendKeys(resource.getTitle());
		this.introduction.sendKeys(resource.getIntroduction());
		this.content.sendKeys(resource.getContent());
		this.newsItemAuthorBox.sendKeys(resource.getAuthor());
		this.newsItemSourceBox.sendKeys(resource.getSource());
		this.newsItemLocationBox.sendKeys(resource.getLocation());
		this.newsItemDateBox.sendKeys(dateFormatter(resource.getDate()));

		doneChanges();

		Thread.sleep(2000);

		List<WebElement> offlineIndicators = driver.findElements(By.xpath("//span[contains(text(),'Offline')]"));
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(offlineIndicators.get(offlineIndicators.size() - 1)));

		return this;
	}

	/**
	 * Fills created blog author dertails and saves
	 * @param resource
	 * @return MyProjectPage
	 * @throws InterruptedException
	 */
	public MyProjectPage insertAndSaveBlogAuthorDetails(BlogAuthorResource resource) throws InterruptedException {
		
		this.authorNameBox.sendKeys(resource.getName());
		this.authorRoleBox.sendKeys(resource.getRole());
		this.authorDescriptionBox.sendKeys(resource.getDescription());

		clickAddButton();
		
		Select typeList = new Select(authorAccountTypesDropdownBox);
		typeList.selectByVisibleText(resource.getExistingAccount());
		this.authorLinkBox.sendKeys(resource.getExternalLink());

		doneChanges();
		
		Thread.sleep(2000);

		List<WebElement> offlineIndicators = driver.findElements(By.xpath("//span[contains(text(),'Offline')]"));
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(offlineIndicators.get(offlineIndicators.size() - 1)));

		
		return this;
	}

	/**
	 * Fills created blog post details and saves
	 * @param BlogPostResource
	 * @return MyProjectPage
	 * @throws InterruptedException
	 */
	public MyProjectPage insertAndSaveBlogPostDetails(BlogPostResource resource) throws InterruptedException {

		this.title.get(0).sendKeys(resource.getTitle());
		this.introduction.sendKeys(resource.getIntroduction());
		this.content.sendKeys(resource.getContent());
		this.blogPostPublicationDateBox.sendKeys(dateFormatter(resource.getPublicationDate()));
		
		//Set Categories
		Actions actions = new Actions(driver);
		for(String category : resource.getExistingCategories())
			actions.keyDown(Keys.LEFT_CONTROL)
					.click(driver.findElement(By.xpath("//option[contains(text(),'" + category + "')]")))
					.keyUp(Keys.LEFT_CONTROL).build().perform();
		
		//Select Author
		List<WebElement> selectButtons;
		for(int i = 0; i < resource.getExistingAuthorNames().size(); i++){
			selectButtons = driver.findElements(By.xpath("//span[text() = 'Select']"));
			
			selectButtons.get(i).click();
			Thread.sleep(1000);
			
			List<WebElement> elements = driver.findElements(By.xpath("//span[text() = 'blog']"));
			elements.get(elements.size() - 1).click();
			Thread.sleep(1000);
			
			elements = driver.findElements(By.xpath("//span[text() = 'authors']"));
			elements.get(elements.size() - 1).click();
			Thread.sleep(1000);
			
			elements = driver.findElements(By.xpath("//span[text() = '" + resource.getExistingAuthorNames().get(i) + "']"));
			elements.get(elements.size() - 1).click();
			Thread.sleep(1000);
			
			submitForm();
			
			if(i + 1 < resource.getExistingAuthorNames().size())
				addButton.click();
		}
		
		doneChanges();

		Thread.sleep(2000);

		List<WebElement> offlineIndicators = driver.findElements(By.xpath("//span[contains(text(),'Offline')]"));
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(offlineIndicators.get(offlineIndicators.size() - 1)));

		return this;
	}
	
	
	public boolean checkSetToNowFunctionality() {
		String expectedDate = new SimpleDateFormat("MM/dd/yyy").format(Calendar.getInstance().getTime());
		setToNowButtons.get(0).click();
		String actualDate = startDate.getText();
		// TODO
		System.out.println("Expected Date: " + expectedDate);
		System.out.println("Actual Date: " + actualDate);

		return expectedDate.equals(actualDate) ? true : false;
	}

	/**
	 * Closes given document 
	 * @param documentName
	 * @return MyProjectPage
	 */
	public MyProjectPage closeGivenDocuments(String documentName) {
		driver.findElement(By.xpath("//div[@class='hippo-tabs-documents']//span[text() ='" + documentName + "']//..//..//a[@class = 'hippo-tabs-documents-close']")).click();
		return this;
	}

	/**
	 * Returns document title
	 * @return String
	 */
	public String getDocumentTitle() {
		return documentTitle.get(documentTitle.size() - 1).getText();
	}

	/**
	 * Returns document introduction
	 * @return String
	 */
	public String getDocumentIntroduction() {
		return documentIntroduction.get(documentIntroduction.size() - 1).getText();
	}

	/**
	 * Returns document content
	 * @return String
	 */
	public String getDocumentContent() {
		return documentContent.get(documentContent.size() - 1).getText();
	}

	/**
	 * Returns event start date
	 * @return String
	 */
	public String getEventStartDate() {
		return eventStartAndEndDate.get(0).getText();
	}

	/**
	 * Returns event end date
	 * @return String
	 */
	public String getEventEndDate() {
		return eventStartAndEndDate.get(1).getText();
	}

	/**
	 * Returns document content
	 * @return String
	 */
	public String getDocumentLocation() {
		return documentLocation.get(documentLocation.size() - 1).getText();
	}
	
	/**
	 * Returns news item author
	 * @return String
	 */
	public String getNewsItemAuthor() {
		return newsItemAuthor.get(newsItemAuthor.size() - 1).getText();
	}
	
	/**
	 * Returns news item source
	 * @return String
	 */
	public String getNewsItemSource() {
		return newsItemSource.get(newsItemSource.size() - 1).getText();
	}
	
	/**
	 * Returns blog author name
	 * @return String
	 */
	public String getAuthorName(){
		return authorName.getText();
	}
	
	/**
	 * Returns blog author role
	 * @return String
	 */
	public String getAuthorRole() {
		return authorRole.getText();
	}

	/**
	 * Returns blog author description
	 * @return String
	 */
	public String getAuthorDescription() {
		return authorDescription.getText();
	}
	
	/**
	 * Returns blog author account type
	 * @return String
	 */
	public String getAuthorAccount() {
		return authorAccountType.getText();
	}
	
	/**
	 * Returns blog author link
	 * @return String
	 */
	public String getAuthorAccountLink() {
		return authorLink.getText();
	}
	
	/**
	 * Returns blog author authors
	 * @return List<String>
	 */
	public List<String> getBlogPostAuthor() {
		List<String> authors = new ArrayList<String>();
		for(WebElement element : blogPostAuthors)
			authors.add(element.getText());
		
		return authors;
	}
	
	/**
	 * Returns blog pos publication date
	 * @return String
	 */
	public String getBlogPostPublicationDate() {
		return blogPostPublicationDate.getText();
	}
	
	/**
	 * Returns blog post categories
	 * @return List<String>
	 */
	public List<String> getBlogPostCategories() {
		List<String> categories = new ArrayList<String>();
		for(WebElement element : blogPostCategory)
			categories.add(element.getText());
		
		return categories;
	}
	
	/**
	 * Send keys to news title and news item date box
	 * @return String
	 */
	public DocumentsPage insertAndSaveMandatoryNewsDetail(String title, Calendar date) {
		this.title.get(0).sendKeys(title);
		this.newsItemDateBox.sendKeys(dateFormatter(date));
		doneChanges();

		return this;
	}
}
