package pageFactory.cms.contentPage;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.opentelemetry.internal.Utils;

public class DocumentsPage extends ContentPage{
	
	@FindBy(xpath = "//span[contains(text(),'My Project')]")
	WebElement myProjectFolder;
	
	@FindBy(xpath = "//span[contains(text(),'administration')]")
	WebElement administrationFolder;
	
	@FindBy(xpath = "//span[contains(text(),'labels')]")
	WebElement labelsFolder;
	
	@FindBy(xpath = "//span[@title='Edit']")
	List<WebElement> editButtonList;
	
	@FindBy(xpath = "//span[contains(text(),'Publication')]")
	WebElement publication;
	
	@FindBy(xpath = "//span[contains(text(),'Take offline...')]")
	WebElement takeOffline;
	
	@FindBy(xpath = "//input[@value='Take offline']")
	WebElement takeOfflinePopUp;
	
	@FindBy(xpath = "//span[contains(text(),'Schedule to take offline...')]")
	WebElement scheduleToTakeOffline;
	
	@FindBy(xpath = "//span[contains(text(),'Publish')]")
	WebElement publish;
	
	@FindBy(xpath = "//span[contains(text(),'Schedule publication...')]")
	WebElement scheduleToPublish;
	
	@FindBy(xpath = "//span[contains(text(),'A previous version is live')]")
	WebElement updateSuccessfulIndicator;
	
	@FindBy(xpath = "//li[@class = 'menu-item']//span[contains(text(),'Document')]")
	WebElement documentButton;
	
	@FindBy(xpath = "//span[contains(text(),'Show revision history...')]")
	WebElement revisionHistory;
	
	@FindBy(xpath = "//span[@title='Restore']")
	WebElement restore;
	
	@FindBy(xpath = "//a[@title='Go to last page']")
	List<WebElement> goToLastPage;
	
	@FindBy(xpath = "//span[text() = 'Document']")
	List<WebElement> documents;
	
	@FindBy(xpath = "//span[contains(text(), 'Delete...')]")
	WebElement deleteDocument;
	
	//span[contains(text(),'Rename...')]
	//span[contains(text(),'Copy...')]
	//span[contains(text(),'Move...')]
	//span[contains(text(),'Delete...')]
	//span[contains(text(),'Show referring documents...')]
	//span[contains(text(),'Info...')]
	
	//span[contains(text(),'View')]
	
	@FindBy(xpath = "//span[contains(text(),'Publish (request)')]")
	WebElement publishRequest;
	
	@FindBy(xpath = "//span[contains(text(),'Schedule publication (request)...')]")
	WebElement schedulePublishRequest;

	@FindBy(xpath = "//span[contains(text(),'Offline')]")
	List<WebElement> offlineIndicator;
	
	@FindBy(xpath = "//span[contains(text(),'Publication request')]")
	WebElement publicationRequestIndicator;
	
	@FindBy(xpath = "//span[text() = 'Requests']")
	WebElement requests;
	
	@FindBy(xpath = "//span[contains(text(),'Accept request')]")
	WebElement acceptRequest;
	
	@FindBy(xpath = "//span[contains(text(),'Reject request')]")
	WebElement rejectRequest;
	
	@FindBy(xpath = "//span[contains(text(),'Start date')]//..//..//div[@class = 'hippo-editor-field-subfield even']//span")
	WebElement startDate;
	
	@FindBy(xpath = "//span[contains(text(),'End date')]//..//..//div[@class = 'hippo-editor-field-subfield even']//span")
	WebElement endDate;
	
	@FindBy(xpath = "//span[contains(text(),'Feedback to author:')]//..//..//..//textarea")
	WebElement rejectFeedbackTextBox;
	
	@FindBy(xpath = "//input[@value='OK']")
	WebElement submitFormButton;
	
	@FindBy(xpath = "//input[@value='Cancel']")
	WebElement cancelFormButton;
	
	@FindBy(xpath = "//input[@value='Delete']")
	WebElement deleteFormButton;
	
	@FindBy(xpath = "//input[@value='Done']")
	WebElement doneFormButton;

	@FindBy(xpath = "//span[contains(text(),'Show rejected request...')]")
	WebElement showRejectedRequest;
	
	@FindBy(xpath = "//span[contains(text(),'Delete folder...')]")
	WebElement deleteFolder;

	@FindBy(xpath = "//span[contains(text(),'Publish all in folder...')]")
	WebElement publishAllInFolder;

	@FindBy(xpath = "//span[contains(text(),'Take offline all in folder...')]")
	WebElement takeOfflineAllInFolder;
	
	@FindBy(xpath = "//span[contains(text(),'Administration')]")
	WebElement administrator;
	
	@FindBy(xpath = "//span[contains(text(),'Unlock')]")
	WebElement unlockDocument;

	@FindBy(xpath = "//span[contains(text(),'Add')]")
	WebElement addButton;
	
	@FindBy(xpath = "//span[text() = 'Key']//..//..//input")
	List<WebElement> keyInputs;
	
	@FindBy(xpath = "//span[text() = 'Value']//..//..//input")
	List<WebElement> valueInputs;
	
	@FindBy(xpath = "//*[name()='use' and @*='#hi-minus-circle-m']")
	List<WebElement> offlineDocumentsImage;
	
	By doneButton = By.xpath("//span[contains(text(),'Done')]");
	By saveButton = By.xpath("//span[contains(text(),'Save')]");
	By cancelButton = By.xpath("//span[contains(text(),'Cancel')]");
	By discardButton = By.xpath("//input[@value = 'Discard']");

	

	public DocumentsPage(WebDriver driver){
		super(driver);
		PageFactory.initElements(driver, this);
	}

	/**
	 * Clicks MyProject folder
	 * @return MyProjectPage
	 */
	public MyProjectPage clickMyProjectFolder() {
		myProjectFolder.click();
		return new MyProjectPage(driver);
	}
	
	/**
	 * Clicks administration folder
	 * @return DocumentsPage
	 */
	public DocumentsPage clickAdministrationFolder() {
		administrationFolder.click();
		return this;
	}
	
	/**
	 * Clicks Labels folder
	 * @return LabelsPage
	 */
	public LabelsPage clickLabelsFolder() {
		labelsFolder.click();
		return new LabelsPage(driver);
	}
	
	/**
	 * Clicks Edit document button
	 * @return DocumentsPage
	 */
	public DocumentsPage clickDocumentEditButton() {
		editButtonList.get(editButtonList.size() - 1).click();
		return this;
	}
	
	/**
	 * Clicks Save document button
	 * @return DocumentsPage
	 */
	public DocumentsPage saveChanges() {
		driver.findElement(saveButton).click();
		return this;
	}
	
	/**
	 * Clicks Done(save and close) document button
	 * @return DocumentsPage
	 */
	public DocumentsPage doneChanges() {
		driver.findElement(doneButton).click();
		return this;
	}

	/**
	 * Clicks Cancel button
	 * @return DocumentsPage
	 */
	public DocumentsPage cancelChanges() {
		driver.findElement(cancelButton).click();
		return this;
	}
	
	/**
	 * Check if update made successfully
	 * @return boolean
	 */
	public boolean isUpdateSuccessfull() {
		return updateSuccessfulIndicator.isDisplayed();
	}
	
	/**
	 * Publishes document 
	 * clicks Publication + Publish menu items
	 * @return DocumentsPage
	 */
	public DocumentsPage publishDocument() {
		
		new WebDriverWait(driver, 5).ignoring(StaleElementReferenceException.class)
				.until(ExpectedConditions.elementToBeClickable(publication));
		driver.findElement(By.xpath("//span[contains(text(),'Publication')]")).click();
		
		publish.click();
		return this;
	}
	
	/**
	 * Takes the opened document offline
	 * @return DocumentsPage
	 */
	public DocumentsPage takeDocumentOffline() {
		new WebDriverWait(driver, 5).ignoring(StaleElementReferenceException.class)
		.until(ExpectedConditions.elementToBeClickable(publication));
		driver.findElement(By.xpath("//span[contains(text(),'Publication')]")).click();
		
		takeOffline.click();
		takeOfflinePopUp.click();
		return this;
	}
	
	/**
	 * Click Document menu item
	 * @return DocumentsPage
	 */
	public DocumentsPage clickDocumentButton() {
		documentButton.click();
		return this;
	}
	
	/**
	 * Clicks Revision History menu item
	 * @return DocumentsPage
	 */
	public DocumentsPage clickRevisionHistory() {
		revisionHistory.click();
		return this;
	}
	
	/**
	 * Goes to last page of the Revision History page
	 * @return DocumentsPage
	 */
	public DocumentsPage goToLastPageOfRevisionHistory() {
		goToLastPage.get(goToLastPage.size() - 1 ).click();
		return this;
	}
	
	/**
	 * Clicks first item in Revision History list
	 * @return DocumentsPage
	 * @throws InterruptedException 
	 */
	public DocumentsPage rollBackInitialRevision() throws InterruptedException {
		clickDocumentButton();
		clickRevisionHistory();
		try{
			if(goToLastPage.get(goToLastPage.size() - 1 ).isDisplayed())
				goToLastPageOfRevisionHistory();
		}catch(NoSuchElementException e) {
			
		}
		Thread.sleep(1000);
		
		List<WebElement> elements = driver.findElements(By.xpath("//table[@class = 'hippo-revision-history']//tbody//tr"));

		elements.get(elements.size() - 1).click();
		
		restore();
		
		return this;
	}
	
	/**
	 * Clicks restore button
	 * @return DocumentsPage
	 */
	public DocumentsPage restore() {
		restore.click();
		return this;
	}
	
	/**
	 * Checks if the document is offline
	 * @return boolean
	 */
	public boolean isDocumentOffline() {
		return offlineIndicator.get(offlineIndicator.size() - 1).isDisplayed();
	}
	
	/**
	 * Check if the publication request sent successfully
	 * @return boolean
	 * @throws InterruptedException
	 */
	public boolean isPublicationRequestSuccessful() throws InterruptedException {
		Thread.sleep(1000);
		List<WebElement> elements = driver.findElements(By.xpath("//span[contains(text(),'Publication request')]"));
		WebElement element = elements.get(elements.size() - 1);

		return element.isDisplayed();
	}

	/**
	 * Accepts publication requests
	 * @return DocumentsPage
	 */
	public DocumentsPage acceptPublicationRequest() {
		requests.click();
		acceptRequest.click();
		return this;
	}
	
	/**
	 * Sends publication request
	 * @return DocumentsPage
	 */
	public DocumentsPage sendPublishRequest() {
		List<WebElement> elements = driver.findElements(By.xpath("//span[contains(text(),'Publication')]"));
		WebElement element = elements.get(elements.size() - 1);
		new WebDriverWait(driver, 20).ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(element));
		element.click();
		publishRequest.click();
		return this;
	}
	
	/**
	 * Rejects publication request
	 * @return DocumentsPage
	 */
	public DocumentsPage rejectPublicationRequest() {
		rejectRequest.click();
		return this;
	}

	/**
	 * Check if document state is online or offline
	 * @param documentName
	 * @return boolean
	 */
	public boolean isDocumentStateLive(String documentName) {
		return driver.findElement(By.xpath("//td[@class = 'doclisting-name link']//span[text() ='" + documentName
				+ "']//..//..//..//span[@class = 'state-live']")).isDisplayed();
	}
	
	/**
	 * Gets start date of Event document
	 * @return String
	 */
	public String getStartDateOfEvent() {
		return replaceShortMonthVersion(startDate.getText());
	}
	
	/**
	 * Gets end date of Event document
	 * @return String
	 */
	public String getEndDateOfEvent() {
		return replaceShortMonthVersion(endDate.getText());
	}
	
	/**
	 * Clicks request button
	 * @return DocumentsPage
	 * @throws InterruptedException
	 */
	public DocumentsPage clickRequestButton() throws InterruptedException {
		Thread.sleep(3000);
		new WebDriverWait(driver, 5).ignoring(StaleElementReferenceException.class).until(ExpectedConditions
				.elementToBeClickable(driver.findElements(By.xpath("//span[contains(text(),'Requests')]")).get(1)));
		driver.findElements(By.xpath("//span[contains(text(),'Requests')]")).get(1).click();

		return this;
	}
	
	/**
	 * Sends given feedback and submits rejected publication request
	 * @param feedback
	 * @return DocumentsPage
	 */
	public DocumentsPage writeFeedbackAndSubmit(String feedback) {
		rejectFeedbackTextBox.sendKeys(feedback);
		submitFormButton.click();
		return this;
	}
	
	/**
	 * Checks if rejection publication requests is successful
	 * @return boolean
	 */
	public boolean isRejectSuccessful() {
		List<WebElement> elements = driver.findElements(By.xpath("//span[contains(text(),'Rejected request')]"));
		if(elements.size() == 0)
			return false;
					
		return elements.get(elements.size() - 1).isDisplayed(); 
	}
	
	/**
	 * Opens sent rejection feedback
	 */
	public DocumentsPage showRejectedRequest() {
		requests.click();
		showRejectedRequest.click();
		return this;
	}

	/**
	 * Checks if the rejection feedback sent correctly
	 * @param feedback
	 * @return boolean
	 */
	public boolean isRejectFeedbackCorrect(String feedback) {
		return driver.findElement(By.xpath("//p[contains(text(),'" + feedback + "')]")).isDisplayed();
	}
	
	/**
	 * Submits form
	 */
	public DocumentsPage submitForm() {
		submitFormButton.click();
		return this;
	}
	
	/**
	 * Checks if document is opened
	 * @return boolean
	 */
	public boolean isDocumentOpened() {
		if(documents.size() == 0)
			return false;
		return documents.get(documents.size() - 1).isDisplayed();
	}
	
	/**
	 * Checks if all documents are offline under opened folder
	 * @return boolean
	 */
	public boolean isAllDocumentsOffline(int documentAmount) {
		return offlineDocumentsImage.size() == documentAmount ? true : false;
	}

	/**
	 * Checks if given document is deleted
	 * @param documentName
	 * @return boolean
	 */
	public boolean isDocumentDeleted(String documentName) {
		try {
			return driver.findElement(By.xpath("//span[text() = '" + documentName + "')]")).isDisplayed() ? false : true;		
		} catch (NoSuchElementException e) {
			return true;
		}
	}

	/**
	 * Deletes currently opened document
	 * @return
	 */
	public DocumentsPage deleteOpenedDocument() {
		documents.get(documents.size() - 1).click();
		deleteDocument.click();
		deleteFormButton.click();
		return this;
	}
	
	/**
	 * Checks if document is in use
	 * @param user
	 * @return boolean
	 */
	public boolean isDocumentInUse(String user) {
		try{
			return driver.findElement(By.xpath("//div[@class = 'hippo-toolbar-status']//span[@title=\"In use by '" + user + "'\"]")).isDisplayed();
		}catch (NoSuchElementException e) {
			return false;
		}
	}

	/**
	 * Checks if Administration button exists or not
	 * @return boolean
	 */
	public boolean isAdministrationButtonExists() { 
		return driver.findElements(By.xpath("//span[contains(text(),'Administration')]")).size() != 0 ? true : false;
	}

	/**
	 * Clicks Administration button
	 * @return DocumentsPage
	 */
	public DocumentsPage clickAdministratorButton() {
		administrator.click();
		return this;
	}
	
	/**
	 * Clicks Unlock button
	 * @return
	 */
	public DocumentsPage clickUnlockButton() {
		unlockDocument.click();
		return this;
	}
	
	/**
	 * Discards changes
 	 * @return DocumentsPage
	 */
	public DocumentsPage discardChanges() {
		cancelChanges();
		driver.findElement(discardButton).click();
		return this;
	}
	
	/**
	 * Clicks add button
	 * @return DocumentsPage
	 */
	public DocumentsPage clickAddButton() {
		addButton.click();
		return this;
	}
	
	/**
	 * Insters and saves given key and value
	 * @param key
	 * @param value
	 * @return DocumentsPage
	 * @throws InterruptedException 
	 */
	public DocumentsPage insertKeyValueAndSave(String key, String value) throws InterruptedException {
		Thread.sleep(1000);
		keyInputs.get(keyInputs.size() - 1).sendKeys(key);
		valueInputs.get(valueInputs.size() - 1).sendKeys(value);
		doneChanges();
		
		return this;
	}
	
	/**
	 * Checks given key value exists on the document
	 * @param key
	 * @return blooean
	 */
	public boolean isKeyValuesExists(String key) {
		try {
			return driver.findElement(By.xpath("//div[contains(text(),'"+ key +"')]")).isDisplayed();
		}catch (NoSuchElementException e) {
			return false;
		}
	}
	
	/**
	 * Deletes key value pair by given value name
	 * @param valueName
	 * @return DocumentsPage
	 */
	public DocumentsPage deleteKeyValuePair(String valueName) {
		WebElement folder = driver.findElement(By.xpath("//input[@value = '" + valueName+ "']"));

		Actions builder = new Actions(driver);
		builder.moveToElement(folder).perform();

		WebElement container = driver.findElement(By.xpath("//input[@value = '" + valueName
				+ "']//..//..//..//..//..//..//..//..//..//..//..//a[@class = 'remove-link']"));
		builder.click(container).perform();
		
		doneChanges();
		
		return this;
	}
	
	/**
	 * Returns given date string with short month version
	 * @param date
	 * @return
	 */
	private String replaceShortMonthVersion(String date) {
		String months[]={"January","February","March","April","May","June","July","August","September","October","November","December"};

		 String [] arr = date.split(" ", 2);

		 for(String month : months)
			 if(month.equals(arr[0]))
				 arr[0] = arr[0].substring(0, 3);
		 
		 return arr[0] + " " + arr[1];
	}
	
	/**
	 * Checks if document edition page opened or not
	 * @return boolean
	 */
	public boolean isEditDocumentOpened() {
		return driver.findElement(doneButton).isDisplayed();
	}
	
	/**
	 * Date formatter
	 * @param date
	 * @return String
	 */
	protected String dateFormatter(Calendar date) {
		return new SimpleDateFormat("MM/dd/yyy").format(date.getTime());
	}

	/**
	 * Returns hours of the Calendar object
	 * @param date
	 * @return String
	 */
	protected String getHours(Calendar date) {
		return String.valueOf(date.get(Calendar.HOUR));
	}

	/**
	 * Returns minutes of the Calendar object
	 * @param date
	 * @return getMinutes
	 */
	protected String getMinutes(Calendar date) {
		return String.valueOf(date.get(Calendar.MINUTE));
	}

}