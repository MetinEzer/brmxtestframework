package pageFactory.cms.contentPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageFactory.base.BasePage;

public class ContentPage extends BasePage{

	@FindBy(xpath = "//span[contains(text(),'Content')]")
	WebElement contentPageIndicator;

	@FindBy(xpath = "//div[@class='selectric']")
	WebElement contentOptionsCursor;
	
	@FindBy(xpath = "//li[@data-index = '0']")
	WebElement documentOption;
	
	@FindBy(xpath = "//li[@data-index = '1']")
	WebElement imageOption;
	
	@FindBy(xpath = "//li[@data-index = '2']")
	WebElement assetOption;
	
	@FindBy(xpath = "//li[@data-index = '3']")
	WebElement documentTypeOption;
	
	public ContentPage(WebDriver driver){
		super(driver);
		PageFactory.initElements(driver, this);
	}

	/**
	 * Check if user is on CMS Content Page
	 * @return boolean
	 */
	public boolean isContentPage() {
		return contentPageIndicator.isDisplayed();
	}

	/**
	 * Selects Documents option from dropdown list
	 * @return
	 */
	public DocumentsPage selectDocumentOption() {
		
		driver.switchTo().frame(driver.findElement(By.tagName("iframe")));
		
	    WebDriverWait wait = new WebDriverWait(driver, 20);
	    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='selectric']")));
 
	    contentOptionsCursor.click();
		documentOption.click();	
		
		return new DocumentsPage(driver);
	}
}