package pageFactory.cms.contentPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LabelsPage extends DocumentsPage{

	@FindBy(xpath = "//span[@title='Global']")
	WebElement globalDocument;
	
	@FindBy(xpath = "//a[@title = 'Edit']")
	WebElement footerTextEditButton;
	
	@FindBy(xpath = "//span[@title='Homepage']")
	WebElement homePageDocument;
	
	@FindBy(xpath = "//span[@title='Page not found']")
	WebElement pageNotFoundDocument;
	
	@FindBy(xpath = "//span[@title='Pagination']")
	WebElement PaginationDocument;
	
	@FindBy(xpath = "//span[@title='Blog']")
	WebElement blogDocument;
	
	@FindBy(xpath = "//span[@title='Facets']")
	WebElement facetsDocument;
	
	By footerValueTextBox = By.xpath("//div[@class = 'input']//input[@type='text']");
	
	public LabelsPage(WebDriver driver){
		super(driver);
		PageFactory.initElements(driver, this);
	}

	/**
	 * Checks if labels folder is displayed
	 * @return boolean
	 */
	public boolean isLabelsFolderDisplayed() {
		return homePageDocument.isDisplayed();
	}
	
	/**
	 * Opens global document
	 * @return LabelsPage
	 */
	public LabelsPage openGlobalDocument() {
		globalDocument.click();
		return this;
	}
	
	/**
	 * Opens HomePage document
	 * @return LabelsPage
	 */
	public LabelsPage openHomePageDocument() {
		homePageDocument.click();
		return this;
	}
	
	/**
	 * Opens Page Not Found document
	 * @return LabelsPage
	 */
	public LabelsPage openPageNotFoundDocument() {
		pageNotFoundDocument.click();
		return this;
	}
	
	/**
	 * Opens Blog document
	 * @return LabelsPage
	 */
	public LabelsPage openBlogDocument() {
		blogDocument.click();
		return this;
	}
	
	/**
	 * Opens Facets document
	 * @return LabelsPage
	 */
	public LabelsPage openFacetsDocument() {
		facetsDocument.click();
		return this;
	}
	/**
	 * Clicks Edit Global Footer Text button
	 * @return LabelsPage
	 */
	public LabelsPage clickEditGlobalFooterText() {
		footerTextEditButton.click();
		return this;
	}
	
	/**
	 * Get current footer text value
	 * @return string
	 */
	public String getCurrentFooterTextValue() {
		return driver.findElement(footerValueTextBox).getAttribute("text");
	}
	
	/**
	 * Updates footer text value 
	 * @param footerTextValue
	 * @return
	 */
	public LabelsPage updateFooterTextValue(String footerTextValue) {
		driver.findElement(footerValueTextBox).clear();
		driver.findElement(footerValueTextBox).sendKeys(footerTextValue);
		return this;
	}
}
