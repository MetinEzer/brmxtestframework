package pageFactory.cms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import pageFactory.base.BasePage;

public class HomePage extends BasePage{

	@FindBy(xpath = "//span[contains(text(),'Home')]")
	WebElement homePageIndicator;
	
	public HomePage(WebDriver driver){
		super(driver);
		PageFactory.initElements(driver, this);
	}

	/**
	 * Check if user is on CMS Home Page
	 * @return boolean
	 */
	public boolean isHomePage() {
		return homePageIndicator.isDisplayed();
	}
	
	/**
	 * To check if publication request is exist on the home page.
	 * 
	 * @param documentObsoletePath : example: events/2020/10/breakfast
	 * @return boolean
	 */
	public boolean isPublicationRequestExist(String documentObsoletePath) {
		return driver.findElement(By
				.xpath("//span[contains(text(),'Pending Requests')]//..//..//a[@title = '/content/documents/myproject/"
						+ documentObsoletePath + "']"))
				.isDisplayed();
	}
	
	/**
	 * Go to publication requested document via given path
	 * 
	 * @param documentObsoletePath example: events/2020/10/breakfast
	 */
	public void goToPublicationRequest(String documentObsoletePath) {
		driver.findElement(By
				.xpath("//span[contains(text(),'Pending Requests')]//..//..//a[@title = '/content/documents/myproject/"
						+ documentObsoletePath + "']"))
				.click();
	}
	
	/**
	 * Navigates tp document page via given path from Activity Stream
	 * 
	 * @param documentObsoletePath example: events/2020/10/breakfast
	 */
	public void navigateToDocumentFromActivtyStream(String documentObsoletePath) {
		driver.findElement(By
				.xpath("//a[@title = '/content/documents/myproject/"
						+ documentObsoletePath + "']"))
				.click();
	}
	
	/**
	 * isRejectedPublicationRequestExist finds if the request is rejected
	 * @param documentObsoletePath example: events/2020/10/breakfast
	 * @return boolean
	 */
	public boolean isRejectedPublicationRequestIOnActivityStream(String documentObsoletePath) {
		return driver.findElement(By
				.xpath("//a[@title = '/content/documents/myproject/"
						+ documentObsoletePath + "']//span[@class = 'bad'][contains(text(), 'denied')]"))
				.isDisplayed();
	}
	
	/**
	 * Checks if the dropped rejected publication request is shown corrent on the page
	 * @param documentObsoletePath
	 * @return boolean
	 */
	public boolean isDroppedRejectedPublicationRequestIOnActivityStream(String documentObsoletePath) {
		return driver.findElement(By
				.xpath("//a[@title = '/content/documents/myproject/"
						+ documentObsoletePath + "']//span[@class = 'bad'][contains(text(), 'withdrew')]"))
				.isDisplayed();
	}
	
	/**
	 * Checks if published publication request show correctly on the page
	 * @param documentObsoletePath
	 * @return boolean
	 */
	public boolean isAcceptedPublicationRequestExist(String documentObsoletePath) {
		return driver.findElement(By
				.xpath("//a[@title = '/content/documents/myproject/" + documentObsoletePath
						+ "']//span[@class = 'good']"))
				.isDisplayed();
	}
	
	
}
