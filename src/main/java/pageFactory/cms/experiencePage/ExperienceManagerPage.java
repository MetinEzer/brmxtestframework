package pageFactory.cms.experiencePage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import pageFactory.base.BasePage;

public class ExperienceManagerPage extends BasePage{

	public ExperienceManagerPage(WebDriver driver){
		super(driver);
		PageFactory.initElements(driver, this);
	}
}
