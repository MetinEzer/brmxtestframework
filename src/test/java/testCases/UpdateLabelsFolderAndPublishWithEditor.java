package testCases;

import org.testng.Assert;
import org.testng.annotations.Test;

import base.TestBase;
import pageFactory.cms.HomePage;
import pageFactory.cms.LoginPage;
import pageFactory.cms.contentPage.DocumentsPage;
import pageFactory.site.SitePage;
import utils.Utils;

public class UpdateLabelsFolderAndPublishWithEditor extends TestBase{

	private final String footerTextNewValue = "footer_" + Utils.generateRandomString();
	
	@Test (description = "Login with Editor user, update Global document in \"labels\" file, publish document, finally assign it back to initial version")
	public void updateGlobalDocumentAndPublishWithEditor() throws Exception {
		loginPage = new LoginPage(driver);
		homePage = new HomePage(driver);
		documentsPage = new DocumentsPage(driver);

		LogTestStep("Login as Editor");
		loginPage.loginUser(credentials.getEditorUser(), credentials.getEditorPassword());
		
		Assert.assertTrue(homePage.isHomePage(), "Login failed, home page is not loaded as expected");
		LogInfo("User " + "\"" + credentials.getEditorUser() + "\"" + " logged in successfully!");
		
		LogTestStep("Go to Content Page");
		homePage.uncollapseMenu();
		contentPage = homePage.goToContentPage();
		
		Assert.assertTrue(contentPage.isContentPage(), "Content Page is not loaded as expected");
		LogInfo("Content Page loaded successfully");
		
		LogTestStep("Go to labels folder");
		labelsPage = contentPage.selectDocumentOption().clickAdministrationFolder().clickLabelsFolder();
		
		Assert.assertTrue(labelsPage.isLabelsFolderDisplayed(), "Labes folder is not displayed as expected");
		LogInfo("Labels folder displayed successfully");
		
		LogTestStep("Update Global document's footer.text value");
		labelsPage.openGlobalDocument().clickDocumentEditButton();
		labelsPage.clickEditGlobalFooterText().updateFooterTextValue(footerTextNewValue);
		documentsPage.submitForm().doneChanges();
		
		Assert.assertTrue(labelsPage.isUpdateSuccessfull(), "Updated document indicator did not appear!");
		LogInfo("Document updated successfully");
		
		LogTestStep("Check the current Global footer value on the live version");
		Utils.openNewTab(driver, SITE_lANDING_PAGE);
		Utils.switchTabs(driver, SECOND_TAB);
		sitePage = new SitePage(driver);
		String currentGlobalFooterValue = sitePage.getSiteGlobalFooterValue();
		
		LogInfo("Current global footer value: " + currentGlobalFooterValue);
		
		LogTestStep("Publish Global Document with updated value");
		Utils.switchTabs(driver, FIRST_TAB);
		Utils.switchFrame(driver);
		documentsPage.publishDocument();
		
		LogTestStep("Verify if updated document on live version");
		Utils.switchTabs(driver, SECOND_TAB);
		Utils.refreshPage(driver);
		Thread.sleep(3000);
		String updatedGlobalFooterValue = sitePage.getSiteGlobalFooterValue();
		
		Assert.assertEquals(footerTextNewValue, updatedGlobalFooterValue, "Value is not updated!");
		LogInfo("Global footer value is updated successfully!");
		
		LogTestStep("Roll back the updated value to its' initial from Revision History.");
		Utils.switchTabs(driver, FIRST_TAB);
		Utils.switchFrame(driver);
		documentsPage.rollBackInitialRevision();
		documentsPage.publishDocument();		

		LogTestStep("Verify if updated document on live version");
		Utils.switchTabs(driver, SECOND_TAB);
		Thread.sleep(1000);
		Utils.refreshPage(driver);

		Assert.assertEquals(sitePage.getSiteGlobalFooterValue(), currentGlobalFooterValue, "Initial value is not rolled back successfully!");
		LogInfo("Restored successfully!");

	}
}