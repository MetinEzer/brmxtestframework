package testCases;

import org.testng.Assert;
import org.testng.annotations.Test;

import base.TestBase;
import data.DocumentAndFolderTypes;
import data.EventResource;
import data.SiteTabs;
import pageFactory.cms.HomePage;
import pageFactory.cms.LoginPage;
import pageFactory.cms.contentPage.DocumentsPage;
import pageFactory.site.SitePage;
import utils.Utils;

public class AuthorCreatesEventDocumentsAndEditorRejectsAndPublishes extends TestBase{
	
	private String newFolderName;
	private final static int DOCUMENT_NUMBER = 2;
	
	@Test(description = "Author creates two event documents under new events folder, sends publication requests, author accepts first requests and rejects second one.")
	public void authorCreatesEventDocumentsAndEditorRejectsAndPublishes() throws InterruptedException {
		loginPage = new LoginPage(driver);
		homePage = new HomePage(driver);
		documentsPage = new DocumentsPage(driver);
				
		LogTestStep("Login with Author user");
		loginGivenUser(credentials.getAuthorUser(), credentials.getAuthorPassword());

		LogTestStep("Go to Content Page");
		contentPage = homePage.goToContentPage();
		
		Assert.assertTrue(contentPage.isContentPage(), "Content Page is not loaded as expected");
		LogInfo("Content Page loaded successfully");

		LogTestStep("Go to My Project's 'Events' folder");
		myProjectPage = contentPage.selectDocumentOption().clickMyProjectFolder().clickGivenFolder(EVENTS);
		
		LogTestStep("Open Events folder option and create new Event Folder");
		newFolderName = Utils.generateRandomString();
		myProjectPage.createNewFolderUnderGivenFolder(DocumentAndFolderTypes.Event, EVENTS, newFolderName);

		Assert.assertTrue(myProjectPage.isFolderCreated(newFolderName), "Folder creation failed! Folder does not exist in folder tree!");
		LogInfo("Folder creation is successful!");
		
		LogTestStep("First Event creation process");
		EventResource firstEventDetail = EventResource.createDefaultEventDetail();
		createEventDetailsAndPublish(firstEventDetail);
		
		LogTestStep("Second Event creation process");
		EventResource secondEventDetail = EventResource.createDefaultEventDetail();
		createEventDetailsAndPublish(secondEventDetail);
		
		LogTestStep("Go to Home Page and check created publication requests from Pending Request tab.");
		Utils.switchToDefaultContent(driver);
		myProjectPage.goToHomePage();
		Utils.switchFrame(driver);

		String firstObsoletePathAddition = ("events/" + newFolderName + "/" + firstEventDetail.getEventDocumentName()).toLowerCase();
		String secondObsoletePathAddition = ("events/" + newFolderName + "/" + secondEventDetail.getEventDocumentName()).toLowerCase();

		Assert.assertTrue(homePage.isPublicationRequestExist(firstObsoletePathAddition), "First publication request does not exists!");
		Assert.assertTrue(homePage.isPublicationRequestExist(secondObsoletePathAddition), "Second publication request does not exists!");
		LogInfo("Publication requests exist on Pending Request tab!");
		
		LogTestStep("Log out from Author user");
		logOutUser();
		
		LogTestStep("Login with Editor user");
		loginGivenUser(credentials.getEditorUser(), credentials.getEditorPassword());

		LogTestStep("Find first publication request and redirect to Content/Event page");
		Utils.switchFrame(driver);
		homePage.goToPublicationRequest(firstObsoletePathAddition);
		Thread.sleep(2000);
		
		Assert.assertTrue(Utils.getCurrentURL(driver).contains(firstObsoletePathAddition), "Redirection to event document failed!");
		LogInfo("Redirection successfull!");
		
		LogTestStep("Accept first event document publish request");
		myProjectPage.acceptPublicationRequest();
		
		Assert.assertTrue(myProjectPage.isDocumentStateLive(firstEventDetail.getEventDocumentName()), "Document state is not live!");
		LogInfo("Document state is live");
		
		String startDate = myProjectPage.getStartDateOfEvent();
		String endDate = myProjectPage.getEndDateOfEvent();
		
		LogTestStep("Check if event document is live on the site");
		Utils.openNewTab(driver, SITE_lANDING_PAGE);
		Utils.switchTabs(driver, SECOND_TAB);
		sitePage = new SitePage(driver);
		sitePage.clickGivenTab(SiteTabs.Event);

		Assert.assertTrue(sitePage.isEventPresent(firstEventDetail.getTitle()), "Published event is not present on live!");
		LogInfo("Published event is present on live!");
		
		LogTestStep("Got to published event page and check event details.");
		sitePage.goToEventDocument(firstEventDetail.getTitle());
		
		Assert.assertEquals(sitePage.getStartDate(), startDate, "Start date of the event is not as expected!");
		Assert.assertEquals(sitePage.getEndDate(), endDate, "End date of the event is not as expected!");
		Assert.assertEquals(sitePage.getIntroduction(), firstEventDetail.getIntroduction(), "Introduction of the event is not as expected!");
		Assert.assertEquals(sitePage.getContent(), firstEventDetail.getContent(), "Content of the event is not as expected!");
		LogInfo("Event details are correct on the live site!");
		
		LogTestStep("Reject second document publish request and send feedback to Author.");
		Utils.switchTabs(driver, FIRST_TAB);
		Utils.switchFrame(driver);
		Thread.sleep(2000);
		String feedback = "RejectFeedback";
		
		myProjectPage.clickGivenDocument(secondEventDetail.getEventDocumentName()).clickRequestButton().rejectPublicationRequest();
		documentsPage.writeFeedbackAndSubmit(feedback);
		LogInfo("Feedback submitted successfully.");
		
		Assert.assertTrue(documentsPage.isRejectSuccessful(), "Rejection indicator is missin, reject publish failed");
		LogInfo("Reject publish document successfully!");
		
		LogTestStep("Log out Editor user.");
		logOutUser();
		
		LogTestStep("Login with Author user");
		loginGivenUser(credentials.getAuthorUser(), credentials.getAuthorPassword());

		LogTestStep("Find rejected and accepted requests and validate if they exist on requests tab");
		Utils.switchFrame(driver);
		
		Assert.assertTrue(homePage.isRejectedPublicationRequestIOnActivityStream(secondObsoletePathAddition), "Rejected request cannot be found on the list");
		Assert.assertTrue(homePage.isAcceptedPublicationRequestExist(firstObsoletePathAddition), "Accepted request cannot be found on Activity list");
		LogInfo("Rejected request shown in Pending Request and Accepted request shown in Activty Stream on the page!");
		
		LogTestStep("Go to rejected publication request");
		homePage.goToPublicationRequest(secondObsoletePathAddition);
		Thread.sleep(2000);
		
		Assert.assertTrue(Utils.getCurrentURL(driver).contains(secondObsoletePathAddition), "Redirection to event document failed!");
		LogInfo("Redirection successfull!");
		
		Assert.assertTrue(documentsPage.isRejectSuccessful(), "Rejected Request indicator is now shown!");
		LogInfo("Rejected Request indicator is shown!");
		
		LogTestStep("Show rejected request and check feedback and drop request");
		myProjectPage.showRejectedRequest();
		
		Assert.assertTrue(documentsPage.isRejectFeedbackCorrect(feedback), "Reject feedback is not shown!");
		LogInfo("Reject feedback is shown successfully!");
		
		documentsPage.submitForm();
		Thread.sleep(1000);
		Assert.assertFalse(documentsPage.isRejectSuccessful(), "Rejected Request indicator still exists!");
		LogInfo("Request dropped and reject request indicator is deleted successfully!");
		
		LogTestStep("Go to Home Page and check Rejected Request is in denied state on Activity Stream.");
		Utils.switchToDefaultContent(driver);
		myProjectPage.goToHomePage();
		Utils.switchFrame(driver);

		Assert.assertTrue(homePage.isDroppedRejectedPublicationRequestIOnActivityStream(secondObsoletePathAddition), "Rejected request is still on the list");
		Assert.assertTrue(homePage.isAcceptedPublicationRequestExist(firstObsoletePathAddition), "Accepted request is not on the list");
		LogInfo("Rejected request is deleted, accepted request is shown correctly on the list!");
		
		LogTestStep("Log out from Author user");
		logOutUser();
		
		LogTestStep("Login with Editor user");
		loginPage.loginUser(credentials.getEditorUser(), credentials.getEditorPassword());

		Assert.assertTrue(homePage.isHomePage(), "Login failed, home page is not loaded as expected");
		LogInfo("User " + "\"" + credentials.getEditorUser() + "\"" + " logged in successfully!");
		homePage.uncollapseMenu();

		LogTestStep("Go to Events folder");
		Utils.switchFrame(driver);
		homePage.navigateToDocumentFromActivtyStream(firstObsoletePathAddition);
		Thread.sleep(2000);
		
		Assert.assertTrue(Utils.getCurrentURL(driver).contains(firstObsoletePathAddition), "Redirection to event document failed!");
		LogInfo("Redirection successfull!");

		LogTestStep("Take offline all the documents in the folder");
		myProjectPage.takeOfflineAllDocumentsUnderGivenFolder(newFolderName, 1);

		Assert.assertTrue(myProjectPage.isAllDocumentsOffline(DOCUMENT_NUMBER), "All documents are not offline!");
		LogInfo("All documents under " + newFolderName + " folder are offline");
		
		LogTestStep("Try to delete not empty folder");
		Assert.assertTrue(myProjectPage.cannotDeleteFolder(newFolderName), "Folder not empty indicator did not show up!");
		LogInfo("File cannot be deleted since it is not empty!");

		LogTestStep("Delete first document");
		myProjectPage.deleteOpenedDocument();
		Assert.assertTrue(documentsPage.isDocumentDeleted(firstEventDetail.getEventDocumentName()), "Document deletion failed!");
		LogInfo("First document deleted successfully!");

		LogTestStep("Delete second document");
		myProjectPage.clickGivenDocument(secondEventDetail.getEventDocumentName()).deleteOpenedDocument();
		Assert.assertTrue(documentsPage.isDocumentDeleted(secondEventDetail.getEventDocumentName()), "Document deletion failed!");
		LogInfo("Second document deleted successfully!");
		
		LogTestStep("Delete created event folder");
		myProjectPage.deleteFolder(newFolderName);
		
		Assert.assertTrue(myProjectPage.isFolderDeleted(newFolderName), "Folder deletion failed!");
		LogInfo("Folder deletion successfull!");
	}
	
	private void createEventDetailsAndPublish(EventResource resource) throws InterruptedException {
		
		LogTestStep("Create a new Event Document under created '" + newFolderName +"' folder");
		myProjectPage.createNewDocumentUnderGivenFolder(DocumentAndFolderTypes.Event, newFolderName, resource.getEventDocumentName());
		
		Assert.assertTrue(myProjectPage.isDocumentCreated(resource.getEventDocumentName()), "Event creation failed!");
		LogInfo("Event creation successful!");
		
		LogTestStep("Fill created event details and save event.");
		myProjectPage.insertAndSaveEventDetails(resource);
		
		Assert.assertEquals(myProjectPage.getDocumentTitle(), resource.getTitle(), "Wrong event title!");
		Assert.assertEquals(myProjectPage.getDocumentIntroduction(), resource.getIntroduction(), "Wrong event introduction!");
		Assert.assertEquals(myProjectPage.getDocumentContent(), resource.getContent(), "Wrong event content!");
		Assert.assertEquals(myProjectPage.getDocumentLocation(), resource.getLocation(), "Wrong event location!");
		Assert.assertTrue(myProjectPage.isDocumentOffline(), "Event document could not saved!");
		LogInfo("Event detail saved successfully!");

		LogTestStep("Send publish request for the event");
		myProjectPage.sendPublishRequest();
		
		Assert.assertTrue(myProjectPage.isPublicationRequestSuccessful(), "Publication request cannot be created!");
		LogInfo("Publication request sent successfully!");

	}
}
