package testCases;

import org.testng.Assert;
import org.testng.annotations.Test;

import base.TestBase;
import data.DocumentAndFolderTypes;
import data.NewsItemResource;
import pageFactory.cms.HomePage;
import pageFactory.cms.LoginPage;
import pageFactory.cms.contentPage.DocumentsPage;

public class DocumentLockingUnlocking extends TestBase{
	
	private static final String DEFAULT_FOLDER_NAME = "2020";

	@Test(description = "Editor cannot access blocked document created by Author, Admin use previlages to unblock and publish")
	public void documentLockingUnlocking() throws InterruptedException {
		loginPage = new LoginPage(driver);
		homePage = new HomePage(driver);
		documentsPage = new DocumentsPage(driver);
				
		LogTestStep("Login as Author");
		loginGivenUser(credentials.getAuthorUser(), credentials.getAuthorPassword());
		
		LogTestStep("Go to Content Page");
		contentPage = homePage.goToContentPage();
		
		Assert.assertTrue(contentPage.isContentPage(), "Content Page is not loaded as expected");
		LogInfo("Content Page loaded successfully");

		LogTestStep("Go to My Project's 'News' folder");
		myProjectPage = contentPage.selectDocumentOption().clickMyProjectFolder().clickGivenFolder(NEWS);
		
		LogTestStep("Create new News Item under given document");
		NewsItemResource itemResource = NewsItemResource.createDefaultNewsItemDetail();
		myProjectPage.createNewDocumentUnderGivenFolder(DocumentAndFolderTypes.News, DEFAULT_FOLDER_NAME, itemResource.getNewsItemName());
		
		LogTestStep("Enter mandatory News Item detail, save and open edit again");
		myProjectPage.insertAndSaveMandatoryNewsDetail(itemResource.getTitle(), itemResource.getDate());
		documentsPage.clickDocumentEditButton();
		
		LogTestStep("Log out from Author user");
		logOutUser();
		
		LogTestStep("Login with Editor user");
		loginGivenUser(credentials.getEditorUser(), credentials.getEditorPassword());

		LogTestStep("Go to My Project's 'News' folder and open 2020 folder");
		contentPage = homePage.goToContentPage();
		myProjectPage = contentPage.selectDocumentOption().clickMyProjectFolder().clickGivenFolder(NEWS)
				.clickGivenFolder(DEFAULT_FOLDER_NAME);

		LogTestStep("Open News Item created by Author");
		myProjectPage.clickGivenDocument(itemResource.getNewsItemName());
		
		LogTestStep("Checking the locked document");
		Assert.assertTrue(documentsPage.isDocumentInUse(AUTHOR), "Document in use indicator is not present!");
		Assert.assertFalse(documentsPage.isAdministrationButtonExists(), "Administration button should not be available for Editor user");
		LogInfo("\"In use by 'author'\" indicator exists! Document is locked.");
		
		LogTestStep("Log out from Author user");
		logOutUser();
		
		LogTestStep("Login with Admin user");
		loginGivenUser(credentials.getAdminUser(), credentials.getAdminPassword());

		LogTestStep("Go to My Project's 'News' folder and open 2020 folder");
		contentPage = homePage.goToContentPage();
		myProjectPage = contentPage.selectDocumentOption().clickMyProjectFolder().clickGivenFolder(NEWS)
				.clickGivenFolder(DEFAULT_FOLDER_NAME);

		LogTestStep("Open News Item created by Author");
		myProjectPage.clickGivenDocument(itemResource.getNewsItemName());
		
		Assert.assertTrue(documentsPage.isDocumentInUse(AUTHOR), "Document in use indicator is not present!");
		Assert.assertTrue(documentsPage.isAdministrationButtonExists(), "Administration button should be available for Admin user");
		LogInfo("\"In use by 'author'\" indicator and Administrator button exists!");

		LogTestStep("Unlock and discard document");
		documentsPage.clickAdministratorButton().clickUnlockButton().discardChanges();

		Assert.assertFalse(documentsPage.isDocumentInUse(AUTHOR), "Document in use is present!");
		LogInfo("Document is unlocked and \"in use\" indicator removed!");
		
		LogTestStep("Delete created document");
		documentsPage.deleteOpenedDocument();
		Assert.assertTrue(documentsPage.isDocumentDeleted(itemResource.getNewsItemName()), "Document deletion failed!");
		LogInfo("Document is deleted successfully!");
	}
}
