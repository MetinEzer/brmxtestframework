package testCases;

import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import base.TestBase;
import data.BlogAuthorResource;
import data.BlogPostResource;
import data.DateCategories;
import data.DocumentAndFolderTypes;
import data.SiteTabs;
import pageFactory.cms.HomePage;
import pageFactory.cms.LoginPage;
import pageFactory.site.SitePage;
import utils.Utils;

public class PublishingBlogDocumentWithNewBlogAuthor extends TestBase{

	private String folderName;
	private BlogAuthorResource authorResource;
	private BlogPostResource firstBlogPostResource;
	private BlogPostResource secondBlogPostResource;
	private List<String> authors;
	private List<String> categories;
	
	private static final int ONLINE_DOC_NUMBER = 2;
	
	@Test(description = "Editor creates a new blog author and publishes new blog entry, end user verifies")
	public void publishingBlogDocumentWithNewBlogAuthor() throws InterruptedException {
		loginPage = new LoginPage(driver);
		homePage = new HomePage(driver);
		
		LogTestStep("Login with Admin user");
		loginGivenUser(credentials.getAdminUser(), credentials.getAdminPassword());

		LogTestStep("Go to My Project's 'Blog' folder");
		contentPage = homePage.goToContentPage();
		myProjectPage = contentPage.selectDocumentOption().clickMyProjectFolder().clickGivenFolder(BLOG);
		
		LogTestStep("Create new blog author account type");
		myProjectPage.clickGivenDocument(BLOG_ACCOUNT_TYPES);
		
		Assert.assertTrue(myProjectPage.isDocumentOpened(), "Account type document is not opened!");
		LogInfo("Account types document is open.");
		
		String randomNumber = Utils.generateRandomString();
		String accountType[] = {"typeKey" + randomNumber, "typeValue" +randomNumber};
		
		addKeyValuePairAndSave(accountType[0], accountType[1]);
		myProjectPage.closeGivenDocuments(BLOG_ACCOUNT_TYPES);
		
		LogTestStep("Create two new blog categories");
		myProjectPage.clickGivenDocument(BLOG_CATEGORIES);

		Assert.assertTrue(myProjectPage.isDocumentOpened(), "Blog categories document is not opened!");
		LogInfo("Blog Categories document is open.");

		String firstBlogCategory[] = {"firstCategoryKey" + randomNumber, "firstCategoryValue" + randomNumber};
		String secondBlogCategory[] = {"secondCategoryKey" + randomNumber, "secondCategoryValue" + randomNumber};
		addKeyValuePairAndSave(firstBlogCategory[0], firstBlogCategory[1]);
		addKeyValuePairAndSave(secondBlogCategory[0], secondBlogCategory[1]);
		myProjectPage.closeGivenDocuments(BLOG_CATEGORIES);

		categories = new ArrayList<String>();
		categories.add(firstBlogCategory[1]);
		
		LogTestStep("Create new blog author");
		authorResource = BlogAuthorResource.createDefaultBlogAuthorResource(accountType[1]);
		authors = Arrays.asList(authorResource.getName());
		myProjectPage.clickGivenFolder(AUTHORS);
		myProjectPage.createNewDocumentUnderGivenFolder(DocumentAndFolderTypes.BlogAuthor, AUTHOR, authorResource.getName());
		
		Assert.assertTrue(myProjectPage.isDocumentCreated(authorResource.getName()), "Document is not created successfully!");
		LogInfo("Author created successfully.");

		LogTestStep("Fill created blog author details and publish document");
		myProjectPage.insertAndSaveBlogAuthorDetails(authorResource);
		
		Assert.assertEquals(myProjectPage.getAuthorName(), authorResource.getName(), "Blog author name is wrong!");
		Assert.assertEquals(myProjectPage.getAuthorRole(), authorResource.getRole(), "Blog author role is wrong!");
		Assert.assertEquals(myProjectPage.getAuthorDescription(), authorResource.getDescription(), "Blog author description is wrong!");
		Assert.assertEquals(myProjectPage.getAuthorAccount(), authorResource.getExistingAccount(), "Blog author type is wrong!");
		Assert.assertEquals(myProjectPage.getAuthorAccountLink(), authorResource.getExternalLink(), "Blog author link is wrong!");
		LogInfo("Blog author detail saved successfully.");
		
		myProjectPage.publishDocument();
		assertTrue(myProjectPage.isDocumentStateLive(authorResource.getName()), "Author document state is not online!");
		LogInfo("Blog Author published successfully.");
		myProjectPage.closeGivenDocuments(authorResource.getName());
		
		LogTestStep("Create new Blog Folder");
		folderName = "newBlogFolder_" + randomNumber;
		myProjectPage.createNewFolderUnderGivenFolder(DocumentAndFolderTypes.Blog, BLOG, folderName);
		
		Assert.assertTrue(myProjectPage.isFolderCreated(folderName), "Folder creation failed!");
		LogInfo("Folder created successfully.");
		
		LogTestStep("Create first blog post with first blog category");
		firstBlogPostResource = BlogPostResource.createDefaultBlogPostDetail(authors, categories);
		Calendar update = firstBlogPostResource.getPublicationDate();
		update.add(Calendar.DATE, -2);
		firstBlogPostResource.setPublicationDate(update);
		createNewBlogPostDetailsAndPublish(firstBlogPostResource);
		
		LogTestStep("Create second blog post with both blog category and two months old publication date");
		categories.add(secondBlogCategory[1]);
		secondBlogPostResource = BlogPostResource.createDefaultBlogPostDetail(authors, categories);
		update = secondBlogPostResource.getPublicationDate();
		update.add(Calendar.MONTH, -2);
		secondBlogPostResource.setPublicationDate(update);
		createNewBlogPostDetailsAndPublish(secondBlogPostResource);

		LogTestStep("Navigate to Site and precess blog filter verifications");
		Utils.openNewTab(driver, SITE_lANDING_PAGE);
		Utils.switchTabs(driver, SECOND_TAB);
		sitePage = new SitePage(driver);
		
		LogTestStep("Open blog tab on the Site, find posts without filter");
		sitePage.clickGivenTab(SiteTabs.Blog);
		
		LogTestStep("Verify first blog post and verify details");
		findBlogPostOnSitePageAndCheckDetails(firstBlogPostResource);
		
		LogTestStep("Verify first blog post and verify details");
		findBlogPostOnSitePageAndCheckDetails(secondBlogPostResource);
		
		LogTestStep("Filter author name and verify blog post");
		checkBlogPostExistInAuthorFilter(firstBlogPostResource);
		checkBlogPostExistInAuthorFilter(secondBlogPostResource);		
		
		LogTestStep("Filter categories and verify blog post");
		//TODO: Due to known Issue!
		LogKnownIssue("Category key names are published instead of key value names!");
		checkBlogPostExistInCategoriesFilter(firstBlogPostResource, firstBlogCategory[0]);
		checkBlogPostExistInCategoriesFilter(secondBlogPostResource, secondBlogCategory[0]);
		checkBlogPostExistInCategoriesFilter(secondBlogPostResource, firstBlogCategory[0]);
		
		LogTestStep("Filter date and verify blog post");
		checkBlogPostExistInDateFilter(firstBlogPostResource, DateCategories.LastSevenDays);
		checkBlogPostExistInDateFilter(secondBlogPostResource, DateCategories.LastThreeMonths);
		
		LogTestStep("Open created blog posts and verify blog post details");
		
		LogTestStep("Take offline created blog posts and delete.");
		Utils.switchTabs(driver, FIRST_TAB);
		Utils.switchFrame(driver);
		myProjectPage.takeOfflineAllDocumentsUnderGivenFolder(folderName, ONLINE_DOC_NUMBER);
		
		Assert.assertTrue(myProjectPage.isAllDocumentsOffline(ONLINE_DOC_NUMBER), "All documents are not offline!");
		LogInfo("All documents under " + folderName + " folder are offline");

		LogTestStep("Delete first blog post");
		deleteDocument(firstBlogPostResource.getBlogPostName());

		LogTestStep("Delete second blog post");
		deleteDocument(secondBlogPostResource.getBlogPostName());

		LogTestStep("Take offline and delete created blog author");
		myProjectPage.clickGivenFolder(AUTHORS).clickGivenDocument(authorResource.getName()).takeDocumentOffline();
		
		Assert.assertTrue(myProjectPage.isDocumentOffline(), "Unpublish blog post is failed!");
		LogInfo("Blog post unpublished successfully.");

		deleteDocument(authorResource.getName());
		
		LogTestStep("Delete created account type");
		myProjectPage.clickGivenFolder(BLOG).clickGivenDocument(BLOG_ACCOUNT_TYPES);
		
		Assert.assertTrue(myProjectPage.isDocumentOpened(), "Edit screen is not opened!");
		LogInfo("Document opened.");
		
		deleteBlogKeyValues(accountType[1]);
		myProjectPage.closeGivenDocuments(BLOG_ACCOUNT_TYPES);
		
		LogTestStep("Delete created blog categories");
		myProjectPage.clickGivenFolder(BLOG).clickGivenDocument(BLOG_CATEGORIES);

		Assert.assertTrue(myProjectPage.isDocumentOpened(), "Edit screen is not opened!");
		LogInfo("Document edition screen opened.");
		
		deleteBlogKeyValues(firstBlogCategory[1]);
		deleteBlogKeyValues(secondBlogCategory[1]);
		myProjectPage.closeGivenDocuments(BLOG_CATEGORIES);


		LogTestStep("Delete blog post folder");
		myProjectPage.clickGivenFolder(BLOG).deleteFolder(folderName);
		
		Assert.assertTrue(myProjectPage.isFolderDeleted(folderName), "Folder deletion failed!");
		LogInfo("Folder deleted successfully.");
	}
	
	private void addKeyValuePairAndSave(String key, String value) throws InterruptedException {
		myProjectPage.clickDocumentEditButton().clickAddButton().insertKeyValueAndSave(key, value);
		Assert.assertTrue(myProjectPage.isKeyValuesExists(key), "Account type key and value does not exists!");
		LogInfo("Account type is created! Key: " + key + ", Value: " + value);
	}
	
	private void createNewBlogPostDetailsAndPublish(BlogPostResource resource) throws InterruptedException {
		myProjectPage.createNewDocumentUnderGivenFolder(DocumentAndFolderTypes.Blog, folderName, resource.getBlogPostName());
		
		Assert.assertTrue(myProjectPage.isDocumentCreated(resource.getBlogPostName()), "Blog post creation failed!");
		LogInfo("Blog post created successfully.");
		
		myProjectPage.insertAndSaveBlogPostDetails(resource);
		
		Assert.assertEquals(myProjectPage.getDocumentTitle(), resource.getTitle(), "Wrong blog post title!");
		Assert.assertEquals(myProjectPage.getDocumentIntroduction(), resource.getIntroduction(), "Wrong blog post introduction!");
		Assert.assertEquals(myProjectPage.getDocumentContent(), resource.getContent(), "Wrong blog post content!");
		for(int i = 0; i < authors.size(); i++)
			Assert.assertEquals(myProjectPage.getBlogPostAuthor().get(i), resource.getExistingAuthorNames().get(i), "Wrong blog post author!");
		for(int i = 0; i < categories.size(); i++)
			Assert.assertEquals(myProjectPage.getBlogPostCategories().get(i), resource.getExistingCategories().get(i), "Wrong blog post category!");

		LogTestStep("Send publish request for the blog post");
		myProjectPage.publishDocument();
		
		Assert.assertTrue(myProjectPage.isDocumentStateLive(resource.getBlogPostName()), "Blog post state is not live!");
		LogInfo("Blog post published successfully.");
	
		myProjectPage.closeGivenDocuments(resource.getBlogPostName());
	}
	
	private void findBlogPostOnSitePageAndCheckDetails(BlogPostResource resource) {
		Assert.assertTrue(sitePage.isBlogPostExist(resource.getTitle()), "Blog post does not exist on the page!");
		LogInfo("Blog post (" + resource.getTitle() + ") found.");
		
		sitePage.clickBlogPost(resource.getTitle());
		LogInfo("Blog post is opened");
		
		Assert.assertEquals(sitePage.getBlogPostTitle(), resource.getTitle(), "Blog post title is wrong!");
		Assert.assertTrue(sitePage.getBlogPostAuthor().contains(resource.getExistingAuthorNames().get(0)), "Blog post author is wrong!");
		Assert.assertEquals(sitePage.getBlogPostIntroduction(), resource.getIntroduction(), "Blog post introduction is wrong!");
		Assert.assertEquals(sitePage.getBlogPostContent(), resource.getContent(), "Blog post content is wrong!");
		LogInfo("Blog post detail is correct.");
		
		sitePage.backToBlogPosts();
		Assert.assertEquals(driver.getCurrentUrl(), SITE_BLOG_PAGE, "Returning blog posts page failed!");
		LogInfo("Returned to blog posts.");

	}
	
	private void checkBlogPostExistInAuthorFilter(BlogPostResource resource) {
		for(String author : resource.getExistingAuthorNames()) {
			Assert.assertTrue(sitePage.isBlogPostUnderCorrectAuthor(author,resource.getTitle()), "Blog post does not exist on the page!");
			LogInfo("Blog post (" + resource.getTitle() + ") found in author (" + author + ").");
			
			sitePage.unselectFilter();
		}
	}
	
	private void checkBlogPostExistInCategoriesFilter(BlogPostResource resource, String category) {
		for(String categoryValue : resource.getExistingCategories()) {
			Assert.assertTrue(sitePage.isBlogPostUnderCorrectCategory(category, resource.getTitle()), "Blog post does not exist on the page!");
			LogInfo("Blog post (" + resource.getTitle() + ") found in category (" + category + ").");
			
			sitePage.unselectFilter();
		}
	}
	
	private void checkBlogPostExistInDateFilter(BlogPostResource resource, DateCategories date) {
		Assert.assertTrue(sitePage.isBlogPostUnderCorrectDate(date, resource.getTitle()), "Blog post does not exist on the page!");
		LogInfo("Blog post (" + resource.getTitle() + ") found in correct Date category.");
		
		sitePage.unselectFilter();
	}
	
	private void deleteBlogKeyValues(String keyValue) throws InterruptedException {
		myProjectPage.clickDocumentEditButton().deleteKeyValuePair(keyValue);
		Assert.assertTrue(!myProjectPage.isKeyValuesExists(keyValue), "Blog key value " + keyValue + "deletion failed!");
		LogInfo("Blog key value (" + keyValue + ") deleted successfully.");
	}
	
	private void deleteDocument(String documentName) throws InterruptedException {
		
		myProjectPage.clickGivenDocument(documentName).deleteOpenedDocument();
		
		Assert.assertTrue(myProjectPage.isDocumentDeleted(documentName), "Document deletion failed!");
		LogInfo("Second blog post deleted successfully!");

	}
}


