package base;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import utils.Common;
import utils.Credentials;

public class TestBase extends Common{
	
	protected  TestBase() {

		try {
			credentials = new Credentials();
		} catch (FileNotFoundException e) {
			System.err.println("Credential file is not located");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected WebDriver initializeDriver(WebDriver driver, String URL) {
	
		String chromeBinary = "src/main/resources/drivers/chrome/chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", chromeBinary);
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-logging");
		options.addArguments("--disable-notifications");
		options.addArguments("use-fake-ui-for-media-stream");
		
		driver = new ChromeDriver(options);
		LogInfo("Browser Opened");
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		LogInfo("Browser maximized"); 
		
		driver.get(URL);
		LogInfo("Directed to url: " + URL);
		
		return driver;
	}

	@BeforeMethod(alwaysRun = true)
	public void beforeMethod(Method method) {
	    Test test = method.getAnnotation(Test.class);
	    LogInfo("============================================Test Start============================================");
	    LogInfo("TEST NAME: " + method.getName());
	    LogInfo("DESCRIPTION : " + test.description());
	    LogInfo("==================================================================================================");
	    
	    LogInfo("Initialize WebDriver");
		driver = initializeDriver(driver, CMS_lANDING_PAGE);
	}
	
	@AfterMethod(alwaysRun = true)
	public void sessionCleanUp() {
	    LogInfo("============================================Session Termination===================================");
	    for(String handle : driver.getWindowHandles()) {
	            driver.switchTo().window(handle);
	            driver.close();
	    }	    	
	    LogInfo("============================================Test End=============================================");

	}
}
