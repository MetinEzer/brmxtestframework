# TestFramework

testFramework is a Java base project that contains E2E regression scenarios for Bloomreach CMS product.

## Installation 
To be able to execute test cases on your local machine, Bloomreach Experience Manager implementation project should be up and running on localhost/8080. Please refer to following steps to achieve brXM up and running or follow the detailed guide line link provided below. 

** 1- Provided steps to run brXM **

Download
```bash
mvn org.apache.maven.plugins:maven-archetype-plugin:2.4:generate -DarchetypeGroupId=org.onehippo.cms7 -DarchetypeArtifactId=hippo-project-archetype -DarchetypeVersion=14.2.2 -DarchetypeRepository=https://maven.onehippo.com/maven2
```

Build
```bash
mvn clean verify
```
Run
```bash
mvn -Pcargo.run -Drepo.path=./storage
```

** 2- Detailed brXM installation guideline **


Bloomreach Experience Manager implementation project detailed guideline:
```bash
https://documentation.bloomreach.com/14/trails/getting-started/get-started.html
```

## Usage 
Import testFramework project as Existing Maven Project and choose tests case that needs to be executed, and run as TestNG

